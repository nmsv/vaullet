# Vaullet

A Solana wallet.

## Build

To build for any of the platforms below you should have:
- Stable Rust toolchain for the target platform (see below).
- Flutter SDK.
- Protocol Buffers compiler (`protoc`).
- Cargo Make plugin.
- Dart Protocol Buffers plugin.

### Android

In addition to the above you would need:
- Android SDK (with NDK).

Toolchain:
```
rustup target add aarch64-linux-android armv7-linux-androideabi x86_64-linux-android i686-linux-android
```

1. Build the rusty part (you can also build in debug mode, but binaries may be just too bloated to be included into the bundle):
    ```
    cargo make --profile release
    ```
2. Generate protobuf files for the dart part (assuming that you have Dart plugin for protoc in `$HOME/.pub-cache/bin/`):
    ```
    protoc --plugin=$HOME/.pub-cache/bin/protoc-gen-dart --dart_out lib/ffi/ proto/vaullet.ffi.proto
    ```
3. Build as any other Flutter project:
    ```
    flutter run --release
    ```

### iOS

*TODO*

## Source Code

[https://codeberg.org/nmsv/vaullet](https://codeberg.org/nmsv/vaullet)

## License

This software is open source and available under the terms of the MPL-2.0.
