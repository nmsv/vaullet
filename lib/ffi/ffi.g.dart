/// bindings for `libvaullet`

import 'dart:ffi';
import 'dart:io';
import 'package:ffi/ffi.dart' as ffi;

// ignore_for_file: unused_import, camel_case_types, non_constant_identifier_names
final DynamicLibrary _dl = _open();
/// Reference to the Dynamic Library, it should be only used for low-level access
final DynamicLibrary dl = _dl;
DynamicLibrary _open() {
  if (Platform.isAndroid) return DynamicLibrary.open('libvaullet.so');
  if (Platform.isIOS) return DynamicLibrary.executable();
  throw UnsupportedError('This platform is not supported.');
}

/// C function `add_signing_account`.
int add_signing_account(
  Pointer<Uint8> account_ptr,
  int account_len,
  int port,
) {
  return _add_signing_account(account_ptr, account_len, port);
}
final _add_signing_account_Dart _add_signing_account = _dl.lookupFunction<_add_signing_account_C, _add_signing_account_Dart>('add_signing_account');
typedef _add_signing_account_C = Int32 Function(
  Pointer<Uint8> account_ptr,
  Uint64 account_len,
  Int64 port,
);
typedef _add_signing_account_Dart = int Function(
  Pointer<Uint8> account_ptr,
  int account_len,
  int port,
);

/// C function `add_watch_account`.
int add_watch_account(
  Pointer<Uint8> account_ptr,
  int account_len,
  int port,
) {
  return _add_watch_account(account_ptr, account_len, port);
}
final _add_watch_account_Dart _add_watch_account = _dl.lookupFunction<_add_watch_account_C, _add_watch_account_Dart>('add_watch_account');
typedef _add_watch_account_C = Int32 Function(
  Pointer<Uint8> account_ptr,
  Uint64 account_len,
  Int64 port,
);
typedef _add_watch_account_Dart = int Function(
  Pointer<Uint8> account_ptr,
  int account_len,
  int port,
);

/// C function `app_init`.
int app_init(
  int port,
  Pointer<ffi.Utf8> docdir,
  Pointer<ffi.Utf8> cachedir,
) {
  return _app_init(port, docdir, cachedir);
}
final _app_init_Dart _app_init = _dl.lookupFunction<_app_init_C, _app_init_Dart>('app_init');
typedef _app_init_C = Int32 Function(
  Int64 port,
  Pointer<ffi.Utf8> docdir,
  Pointer<ffi.Utf8> cachedir,
);
typedef _app_init_Dart = int Function(
  int port,
  Pointer<ffi.Utf8> docdir,
  Pointer<ffi.Utf8> cachedir,
);

/// C function `camera_get_result`.
void camera_get_result(
  int port,
) {
  _camera_get_result(port);
}
final _camera_get_result_Dart _camera_get_result = _dl.lookupFunction<_camera_get_result_C, _camera_get_result_Dart>('camera_get_result');
typedef _camera_get_result_C = Void Function(
  Int64 port,
);
typedef _camera_get_result_Dart = void Function(
  int port,
);

/// C function `camera_submit_image`.
int camera_submit_image(
  Pointer<Uint8> image_ptr,
  int image_len,
  int width,
  int height,
) {
  return _camera_submit_image(image_ptr, image_len, width, height);
}
final _camera_submit_image_Dart _camera_submit_image = _dl.lookupFunction<_camera_submit_image_C, _camera_submit_image_Dart>('camera_submit_image');
typedef _camera_submit_image_C = Int32 Function(
  Pointer<Uint8> image_ptr,
  Uint64 image_len,
  Uint32 width,
  Uint32 height,
);
typedef _camera_submit_image_Dart = int Function(
  Pointer<Uint8> image_ptr,
  int image_len,
  int width,
  int height,
);

/// C function `get_state`.
void get_state(
  int port,
) {
  _get_state(port);
}
final _get_state_Dart _get_state = _dl.lookupFunction<_get_state_C, _get_state_Dart>('get_state');
typedef _get_state_C = Void Function(
  Int64 port,
);
typedef _get_state_Dart = void Function(
  int port,
);

/// C function `get_state_subscribe`.
void get_state_subscribe(
  int port,
) {
  _get_state_subscribe(port);
}
final _get_state_subscribe_Dart _get_state_subscribe = _dl.lookupFunction<_get_state_subscribe_C, _get_state_subscribe_Dart>('get_state_subscribe');
typedef _get_state_subscribe_C = Void Function(
  Int64 port,
);
typedef _get_state_subscribe_Dart = void Function(
  int port,
);

/// C function `log_init`.
void log_init(
  int _port,
) {
  _log_init(_port);
}
final _log_init_Dart _log_init = _dl.lookupFunction<_log_init_C, _log_init_Dart>('log_init');
typedef _log_init_C = Void Function(
  Int64 _port,
);
typedef _log_init_Dart = void Function(
  int _port,
);

/// C function `send_from_active_account`.
int send_from_active_account(
  Pointer<Uint8> new_transaction_ptr,
  int new_transaction_len,
  int port,
) {
  return _send_from_active_account(new_transaction_ptr, new_transaction_len, port);
}
final _send_from_active_account_Dart _send_from_active_account = _dl.lookupFunction<_send_from_active_account_C, _send_from_active_account_Dart>('send_from_active_account');
typedef _send_from_active_account_C = Int32 Function(
  Pointer<Uint8> new_transaction_ptr,
  Uint64 new_transaction_len,
  Int64 port,
);
typedef _send_from_active_account_Dart = int Function(
  Pointer<Uint8> new_transaction_ptr,
  int new_transaction_len,
  int port,
);

/// C function `set_active_account`.
void set_active_account(
  int acc_idx,
) {
  _set_active_account(acc_idx);
}
final _set_active_account_Dart _set_active_account = _dl.lookupFunction<_set_active_account_C, _set_active_account_Dart>('set_active_account');
typedef _set_active_account_C = Void Function(
  Uint64 acc_idx,
);
typedef _set_active_account_Dart = void Function(
  int acc_idx,
);

/// C function `state_subscribe`.
void state_subscribe(
  int port,
) {
  _state_subscribe(port);
}
final _state_subscribe_Dart _state_subscribe = _dl.lookupFunction<_state_subscribe_C, _state_subscribe_Dart>('state_subscribe');
typedef _state_subscribe_C = Void Function(
  Int64 port,
);
typedef _state_subscribe_Dart = void Function(
  int port,
);

/// Binding to `allo-isolate` crate
void store_dart_post_cobject(
  Pointer<NativeFunction<Int8 Function(Int64, Pointer<Dart_CObject>)>> ptr,
) {
  _store_dart_post_cobject(ptr);
}
final _store_dart_post_cobject_Dart _store_dart_post_cobject = _dl.lookupFunction<_store_dart_post_cobject_C, _store_dart_post_cobject_Dart>('store_dart_post_cobject');
typedef _store_dart_post_cobject_C = Void Function(
  Pointer<NativeFunction<Int8 Function(Int64, Pointer<Dart_CObject>)>> ptr,
);
typedef _store_dart_post_cobject_Dart = void Function(
  Pointer<NativeFunction<Int8 Function(Int64, Pointer<Dart_CObject>)>> ptr,
);
