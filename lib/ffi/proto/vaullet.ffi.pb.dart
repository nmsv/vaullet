///
//  Generated code. Do not modify.
//  source: proto/vaullet.ffi.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

export 'vaullet.ffi.pbenum.dart';

class State extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'State', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..pc<Account>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accounts', $pb.PbFieldType.PM, subBuilder: Account.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'activeAccount', $pb.PbFieldType.OU3)
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'transactionLoading')
    ..pc<Transaction>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'transactions', $pb.PbFieldType.PM, subBuilder: Transaction.create)
    ..hasRequiredFields = false
  ;

  State._() : super();
  factory State({
    $core.Iterable<Account>? accounts,
    $core.int? activeAccount,
    $core.bool? transactionLoading,
    $core.Iterable<Transaction>? transactions,
  }) {
    final _result = create();
    if (accounts != null) {
      _result.accounts.addAll(accounts);
    }
    if (activeAccount != null) {
      _result.activeAccount = activeAccount;
    }
    if (transactionLoading != null) {
      _result.transactionLoading = transactionLoading;
    }
    if (transactions != null) {
      _result.transactions.addAll(transactions);
    }
    return _result;
  }
  factory State.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory State.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  State clone() => State()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  State copyWith(void Function(State) updates) => super.copyWith((message) => updates(message as State)) as State; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static State create() => State._();
  State createEmptyInstance() => create();
  static $pb.PbList<State> createRepeated() => $pb.PbList<State>();
  @$core.pragma('dart2js:noInline')
  static State getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<State>(create);
  static State? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Account> get accounts => $_getList(0);

  @$pb.TagNumber(2)
  $core.int get activeAccount => $_getIZ(1);
  @$pb.TagNumber(2)
  set activeAccount($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasActiveAccount() => $_has(1);
  @$pb.TagNumber(2)
  void clearActiveAccount() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get transactionLoading => $_getBF(2);
  @$pb.TagNumber(3)
  set transactionLoading($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTransactionLoading() => $_has(2);
  @$pb.TagNumber(3)
  void clearTransactionLoading() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<Transaction> get transactions => $_getList(3);
}

class Account extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Account', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcHttpAddress')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcWsAddress')
    ..a<$fixnum.Int64>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'balance', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOB(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'canSign')
    ..hasRequiredFields = false
  ;

  Account._() : super();
  factory Account({
    $core.String? name,
    $core.String? address,
    $core.String? rpcHttpAddress,
    $core.String? rpcWsAddress,
    $fixnum.Int64? balance,
    $core.bool? canSign,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (address != null) {
      _result.address = address;
    }
    if (rpcHttpAddress != null) {
      _result.rpcHttpAddress = rpcHttpAddress;
    }
    if (rpcWsAddress != null) {
      _result.rpcWsAddress = rpcWsAddress;
    }
    if (balance != null) {
      _result.balance = balance;
    }
    if (canSign != null) {
      _result.canSign = canSign;
    }
    return _result;
  }
  factory Account.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Account.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Account clone() => Account()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Account copyWith(void Function(Account) updates) => super.copyWith((message) => updates(message as Account)) as Account; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Account create() => Account._();
  Account createEmptyInstance() => create();
  static $pb.PbList<Account> createRepeated() => $pb.PbList<Account>();
  @$core.pragma('dart2js:noInline')
  static Account getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Account>(create);
  static Account? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get address => $_getSZ(1);
  @$pb.TagNumber(2)
  set address($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get rpcHttpAddress => $_getSZ(2);
  @$pb.TagNumber(3)
  set rpcHttpAddress($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRpcHttpAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearRpcHttpAddress() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get rpcWsAddress => $_getSZ(3);
  @$pb.TagNumber(4)
  set rpcWsAddress($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasRpcWsAddress() => $_has(3);
  @$pb.TagNumber(4)
  void clearRpcWsAddress() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get balance => $_getI64(4);
  @$pb.TagNumber(5)
  set balance($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasBalance() => $_has(4);
  @$pb.TagNumber(5)
  void clearBalance() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get canSign => $_getBF(5);
  @$pb.TagNumber(6)
  set canSign($core.bool v) { $_setBool(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasCanSign() => $_has(5);
  @$pb.TagNumber(6)
  void clearCanSign() => clearField(6);
}

class Transaction extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Transaction', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'signature')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'accountAddress')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'timestamp')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OU3)
    ..a<$fixnum.Int64>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'preBalance', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'postBalance', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false
  ;

  Transaction._() : super();
  factory Transaction({
    $core.String? signature,
    $core.String? accountAddress,
    $fixnum.Int64? timestamp,
    $core.int? status,
    $fixnum.Int64? preBalance,
    $fixnum.Int64? postBalance,
  }) {
    final _result = create();
    if (signature != null) {
      _result.signature = signature;
    }
    if (accountAddress != null) {
      _result.accountAddress = accountAddress;
    }
    if (timestamp != null) {
      _result.timestamp = timestamp;
    }
    if (status != null) {
      _result.status = status;
    }
    if (preBalance != null) {
      _result.preBalance = preBalance;
    }
    if (postBalance != null) {
      _result.postBalance = postBalance;
    }
    return _result;
  }
  factory Transaction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Transaction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Transaction clone() => Transaction()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Transaction copyWith(void Function(Transaction) updates) => super.copyWith((message) => updates(message as Transaction)) as Transaction; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Transaction create() => Transaction._();
  Transaction createEmptyInstance() => create();
  static $pb.PbList<Transaction> createRepeated() => $pb.PbList<Transaction>();
  @$core.pragma('dart2js:noInline')
  static Transaction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Transaction>(create);
  static Transaction? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get signature => $_getSZ(0);
  @$pb.TagNumber(1)
  set signature($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSignature() => $_has(0);
  @$pb.TagNumber(1)
  void clearSignature() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get accountAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set accountAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAccountAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearAccountAddress() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get timestamp => $_getI64(2);
  @$pb.TagNumber(3)
  set timestamp($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTimestamp() => $_has(2);
  @$pb.TagNumber(3)
  void clearTimestamp() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get status => $_getIZ(3);
  @$pb.TagNumber(4)
  set status($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatus() => $_has(3);
  @$pb.TagNumber(4)
  void clearStatus() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get preBalance => $_getI64(4);
  @$pb.TagNumber(5)
  set preBalance($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPreBalance() => $_has(4);
  @$pb.TagNumber(5)
  void clearPreBalance() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get postBalance => $_getI64(5);
  @$pb.TagNumber(6)
  set postBalance($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPostBalance() => $_has(5);
  @$pb.TagNumber(6)
  void clearPostBalance() => clearField(6);
}

class NewWatchAccount extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewWatchAccount', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcHttpAddress')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcWsAddress')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..hasRequiredFields = false
  ;

  NewWatchAccount._() : super();
  factory NewWatchAccount({
    $core.String? name,
    $core.String? rpcHttpAddress,
    $core.String? rpcWsAddress,
    $core.String? address,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (rpcHttpAddress != null) {
      _result.rpcHttpAddress = rpcHttpAddress;
    }
    if (rpcWsAddress != null) {
      _result.rpcWsAddress = rpcWsAddress;
    }
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory NewWatchAccount.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewWatchAccount.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewWatchAccount clone() => NewWatchAccount()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewWatchAccount copyWith(void Function(NewWatchAccount) updates) => super.copyWith((message) => updates(message as NewWatchAccount)) as NewWatchAccount; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewWatchAccount create() => NewWatchAccount._();
  NewWatchAccount createEmptyInstance() => create();
  static $pb.PbList<NewWatchAccount> createRepeated() => $pb.PbList<NewWatchAccount>();
  @$core.pragma('dart2js:noInline')
  static NewWatchAccount getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewWatchAccount>(create);
  static NewWatchAccount? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get rpcHttpAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set rpcHttpAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRpcHttpAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearRpcHttpAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get rpcWsAddress => $_getSZ(2);
  @$pb.TagNumber(3)
  set rpcWsAddress($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRpcWsAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearRpcWsAddress() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get address => $_getSZ(3);
  @$pb.TagNumber(4)
  set address($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAddress() => $_has(3);
  @$pb.TagNumber(4)
  void clearAddress() => clearField(4);
}

class NewSigningAccount extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewSigningAccount', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcHttpAddress')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rpcWsAddress')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'seedPhrase')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'passphrase')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'derivationPath')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'encryptionPassword')
    ..hasRequiredFields = false
  ;

  NewSigningAccount._() : super();
  factory NewSigningAccount({
    $core.String? name,
    $core.String? rpcHttpAddress,
    $core.String? rpcWsAddress,
    $core.String? seedPhrase,
    $core.String? passphrase,
    $core.String? derivationPath,
    $core.String? encryptionPassword,
  }) {
    final _result = create();
    if (name != null) {
      _result.name = name;
    }
    if (rpcHttpAddress != null) {
      _result.rpcHttpAddress = rpcHttpAddress;
    }
    if (rpcWsAddress != null) {
      _result.rpcWsAddress = rpcWsAddress;
    }
    if (seedPhrase != null) {
      _result.seedPhrase = seedPhrase;
    }
    if (passphrase != null) {
      _result.passphrase = passphrase;
    }
    if (derivationPath != null) {
      _result.derivationPath = derivationPath;
    }
    if (encryptionPassword != null) {
      _result.encryptionPassword = encryptionPassword;
    }
    return _result;
  }
  factory NewSigningAccount.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewSigningAccount.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewSigningAccount clone() => NewSigningAccount()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewSigningAccount copyWith(void Function(NewSigningAccount) updates) => super.copyWith((message) => updates(message as NewSigningAccount)) as NewSigningAccount; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewSigningAccount create() => NewSigningAccount._();
  NewSigningAccount createEmptyInstance() => create();
  static $pb.PbList<NewSigningAccount> createRepeated() => $pb.PbList<NewSigningAccount>();
  @$core.pragma('dart2js:noInline')
  static NewSigningAccount getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewSigningAccount>(create);
  static NewSigningAccount? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get rpcHttpAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set rpcHttpAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRpcHttpAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearRpcHttpAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get rpcWsAddress => $_getSZ(2);
  @$pb.TagNumber(3)
  set rpcWsAddress($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRpcWsAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearRpcWsAddress() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get seedPhrase => $_getSZ(3);
  @$pb.TagNumber(4)
  set seedPhrase($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSeedPhrase() => $_has(3);
  @$pb.TagNumber(4)
  void clearSeedPhrase() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get passphrase => $_getSZ(4);
  @$pb.TagNumber(5)
  set passphrase($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPassphrase() => $_has(4);
  @$pb.TagNumber(5)
  void clearPassphrase() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get derivationPath => $_getSZ(5);
  @$pb.TagNumber(6)
  set derivationPath($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDerivationPath() => $_has(5);
  @$pb.TagNumber(6)
  void clearDerivationPath() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get encryptionPassword => $_getSZ(6);
  @$pb.TagNumber(7)
  set encryptionPassword($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasEncryptionPassword() => $_has(6);
  @$pb.TagNumber(7)
  void clearEncryptionPassword() => clearField(7);
}

class NewAccountTransaction extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NewAccountTransaction', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'vaullet.ffi'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'destination')
    ..a<$fixnum.Int64>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'amount', $pb.PbFieldType.OU6, defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'password')
    ..hasRequiredFields = false
  ;

  NewAccountTransaction._() : super();
  factory NewAccountTransaction({
    $core.String? destination,
    $fixnum.Int64? amount,
    $core.String? password,
  }) {
    final _result = create();
    if (destination != null) {
      _result.destination = destination;
    }
    if (amount != null) {
      _result.amount = amount;
    }
    if (password != null) {
      _result.password = password;
    }
    return _result;
  }
  factory NewAccountTransaction.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NewAccountTransaction.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NewAccountTransaction clone() => NewAccountTransaction()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NewAccountTransaction copyWith(void Function(NewAccountTransaction) updates) => super.copyWith((message) => updates(message as NewAccountTransaction)) as NewAccountTransaction; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NewAccountTransaction create() => NewAccountTransaction._();
  NewAccountTransaction createEmptyInstance() => create();
  static $pb.PbList<NewAccountTransaction> createRepeated() => $pb.PbList<NewAccountTransaction>();
  @$core.pragma('dart2js:noInline')
  static NewAccountTransaction getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NewAccountTransaction>(create);
  static NewAccountTransaction? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get destination => $_getSZ(0);
  @$pb.TagNumber(1)
  set destination($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDestination() => $_has(0);
  @$pb.TagNumber(1)
  void clearDestination() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get amount => $_getI64(1);
  @$pb.TagNumber(2)
  set amount($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAmount() => $_has(1);
  @$pb.TagNumber(2)
  void clearAmount() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);
}

