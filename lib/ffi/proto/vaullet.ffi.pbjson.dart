///
//  Generated code. Do not modify.
//  source: proto/vaullet.ffi.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use errorDescriptor instead')
const Error$json = const {
  '1': 'Error',
  '2': const [
    const {'1': 'NONE', '2': 0},
    const {'1': 'NULL_POINTER_INPUT', '2': 1},
    const {'1': 'STR_CONVERT_ERROR', '2': 2},
    const {'1': 'PATH_PARSE_ERROR', '2': 3},
    const {'1': 'DOC_DIR_ACCESS_ERROR', '2': 4},
    const {'1': 'CACHE_DIR_ACCESS_ERROR', '2': 5},
    const {'1': 'SPAWN_BLOCKING_ERROR', '2': 6},
    const {'1': 'DATA_DB_OPEN_ERROR', '2': 7},
    const {'1': 'CACHE_DB_OPEN_ERROR', '2': 8},
    const {'1': 'JOIN_ERROR', '2': 9},
    const {'1': 'CONNECTION_ERROR', '2': 10},
    const {'1': 'INCORRECT_PUBLIC_KEY_FORMAT', '2': 11},
    const {'1': 'INCORRECT_KEYPAIR_FORMAT', '2': 12},
    const {'1': 'INCORRECT_ENCODING_FORMAT', '2': 13},
    const {'1': 'INCORRECT_DERIVATION_PATH_FORMAT', '2': 14},
    const {'1': 'UNABLE_TO_DERIVE_KEY', '2': 15},
    const {'1': 'HASH_OUTPUT_BUFFER_ERROR', '2': 16},
    const {'1': 'ACCOUNT_CAN_NOT_SIGN', '2': 17},
    const {'1': 'NO_ACTIVE_ACCOUNT', '2': 18},
    const {'1': 'INCORRECT_TRANSACTION_STATUS', '2': 19},
    const {'1': 'DECRYPTION_ERROR', '2': 20},
    const {'1': 'SIGNING_ERROR', '2': 21},
    const {'1': 'INCORRECT_DESTINATION_ADDRESS_FORMAT', '2': 22},
    const {'1': 'TRANSACTION_SEND_ERROR', '2': 23},
    const {'1': 'INCORRECT_SIGNATURE_FORMAT', '2': 24},
    const {'1': 'OPEN_DATA_TREE_ERROR', '2': 25},
    const {'1': 'UNKNOWN_ERROR', '2': 26},
  ],
};

/// Descriptor for `Error`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List errorDescriptor = $convert.base64Decode('CgVFcnJvchIICgROT05FEAASFgoSTlVMTF9QT0lOVEVSX0lOUFVUEAESFQoRU1RSX0NPTlZFUlRfRVJST1IQAhIUChBQQVRIX1BBUlNFX0VSUk9SEAMSGAoURE9DX0RJUl9BQ0NFU1NfRVJST1IQBBIaChZDQUNIRV9ESVJfQUNDRVNTX0VSUk9SEAUSGAoUU1BBV05fQkxPQ0tJTkdfRVJST1IQBhIWChJEQVRBX0RCX09QRU5fRVJST1IQBxIXChNDQUNIRV9EQl9PUEVOX0VSUk9SEAgSDgoKSk9JTl9FUlJPUhAJEhQKEENPTk5FQ1RJT05fRVJST1IQChIfChtJTkNPUlJFQ1RfUFVCTElDX0tFWV9GT1JNQVQQCxIcChhJTkNPUlJFQ1RfS0VZUEFJUl9GT1JNQVQQDBIdChlJTkNPUlJFQ1RfRU5DT0RJTkdfRk9STUFUEA0SJAogSU5DT1JSRUNUX0RFUklWQVRJT05fUEFUSF9GT1JNQVQQDhIYChRVTkFCTEVfVE9fREVSSVZFX0tFWRAPEhwKGEhBU0hfT1VUUFVUX0JVRkZFUl9FUlJPUhAQEhgKFEFDQ09VTlRfQ0FOX05PVF9TSUdOEBESFQoRTk9fQUNUSVZFX0FDQ09VTlQQEhIgChxJTkNPUlJFQ1RfVFJBTlNBQ1RJT05fU1RBVFVTEBMSFAoQREVDUllQVElPTl9FUlJPUhAUEhEKDVNJR05JTkdfRVJST1IQFRIoCiRJTkNPUlJFQ1RfREVTVElOQVRJT05fQUREUkVTU19GT1JNQVQQFhIaChZUUkFOU0FDVElPTl9TRU5EX0VSUk9SEBcSHgoaSU5DT1JSRUNUX1NJR05BVFVSRV9GT1JNQVQQGBIYChRPUEVOX0RBVEFfVFJFRV9FUlJPUhAZEhEKDVVOS05PV05fRVJST1IQGg==');
@$core.Deprecated('Use stateDescriptor instead')
const State$json = const {
  '1': 'State',
  '2': const [
    const {'1': 'accounts', '3': 1, '4': 3, '5': 11, '6': '.vaullet.ffi.Account', '10': 'accounts'},
    const {'1': 'active_account', '3': 2, '4': 1, '5': 13, '9': 0, '10': 'activeAccount', '17': true},
    const {'1': 'transaction_loading', '3': 3, '4': 1, '5': 8, '10': 'transactionLoading'},
    const {'1': 'transactions', '3': 4, '4': 3, '5': 11, '6': '.vaullet.ffi.Transaction', '10': 'transactions'},
  ],
  '8': const [
    const {'1': '_active_account'},
  ],
};

/// Descriptor for `State`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List stateDescriptor = $convert.base64Decode('CgVTdGF0ZRIwCghhY2NvdW50cxgBIAMoCzIULnZhdWxsZXQuZmZpLkFjY291bnRSCGFjY291bnRzEioKDmFjdGl2ZV9hY2NvdW50GAIgASgNSABSDWFjdGl2ZUFjY291bnSIAQESLwoTdHJhbnNhY3Rpb25fbG9hZGluZxgDIAEoCFISdHJhbnNhY3Rpb25Mb2FkaW5nEjwKDHRyYW5zYWN0aW9ucxgEIAMoCzIYLnZhdWxsZXQuZmZpLlRyYW5zYWN0aW9uUgx0cmFuc2FjdGlvbnNCEQoPX2FjdGl2ZV9hY2NvdW50');
@$core.Deprecated('Use accountDescriptor instead')
const Account$json = const {
  '1': 'Account',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'address', '3': 2, '4': 1, '5': 9, '10': 'address'},
    const {'1': 'rpc_http_address', '3': 3, '4': 1, '5': 9, '10': 'rpcHttpAddress'},
    const {'1': 'rpc_ws_address', '3': 4, '4': 1, '5': 9, '10': 'rpcWsAddress'},
    const {'1': 'balance', '3': 5, '4': 1, '5': 4, '9': 0, '10': 'balance', '17': true},
    const {'1': 'can_sign', '3': 6, '4': 1, '5': 8, '10': 'canSign'},
  ],
  '8': const [
    const {'1': '_balance'},
  ],
};

/// Descriptor for `Account`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List accountDescriptor = $convert.base64Decode('CgdBY2NvdW50EhIKBG5hbWUYASABKAlSBG5hbWUSGAoHYWRkcmVzcxgCIAEoCVIHYWRkcmVzcxIoChBycGNfaHR0cF9hZGRyZXNzGAMgASgJUg5ycGNIdHRwQWRkcmVzcxIkCg5ycGNfd3NfYWRkcmVzcxgEIAEoCVIMcnBjV3NBZGRyZXNzEh0KB2JhbGFuY2UYBSABKARIAFIHYmFsYW5jZYgBARIZCghjYW5fc2lnbhgGIAEoCFIHY2FuU2lnbkIKCghfYmFsYW5jZQ==');
@$core.Deprecated('Use transactionDescriptor instead')
const Transaction$json = const {
  '1': 'Transaction',
  '2': const [
    const {'1': 'signature', '3': 1, '4': 1, '5': 9, '10': 'signature'},
    const {'1': 'account_address', '3': 2, '4': 1, '5': 9, '10': 'accountAddress'},
    const {'1': 'timestamp', '3': 3, '4': 1, '5': 3, '10': 'timestamp'},
    const {'1': 'status', '3': 4, '4': 1, '5': 13, '10': 'status'},
    const {'1': 'pre_balance', '3': 5, '4': 1, '5': 4, '10': 'preBalance'},
    const {'1': 'post_balance', '3': 6, '4': 1, '5': 4, '10': 'postBalance'},
  ],
};

/// Descriptor for `Transaction`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List transactionDescriptor = $convert.base64Decode('CgtUcmFuc2FjdGlvbhIcCglzaWduYXR1cmUYASABKAlSCXNpZ25hdHVyZRInCg9hY2NvdW50X2FkZHJlc3MYAiABKAlSDmFjY291bnRBZGRyZXNzEhwKCXRpbWVzdGFtcBgDIAEoA1IJdGltZXN0YW1wEhYKBnN0YXR1cxgEIAEoDVIGc3RhdHVzEh8KC3ByZV9iYWxhbmNlGAUgASgEUgpwcmVCYWxhbmNlEiEKDHBvc3RfYmFsYW5jZRgGIAEoBFILcG9zdEJhbGFuY2U=');
@$core.Deprecated('Use newWatchAccountDescriptor instead')
const NewWatchAccount$json = const {
  '1': 'NewWatchAccount',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'rpc_http_address', '3': 2, '4': 1, '5': 9, '10': 'rpcHttpAddress'},
    const {'1': 'rpc_ws_address', '3': 3, '4': 1, '5': 9, '10': 'rpcWsAddress'},
    const {'1': 'address', '3': 4, '4': 1, '5': 9, '10': 'address'},
  ],
};

/// Descriptor for `NewWatchAccount`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newWatchAccountDescriptor = $convert.base64Decode('Cg9OZXdXYXRjaEFjY291bnQSEgoEbmFtZRgBIAEoCVIEbmFtZRIoChBycGNfaHR0cF9hZGRyZXNzGAIgASgJUg5ycGNIdHRwQWRkcmVzcxIkCg5ycGNfd3NfYWRkcmVzcxgDIAEoCVIMcnBjV3NBZGRyZXNzEhgKB2FkZHJlc3MYBCABKAlSB2FkZHJlc3M=');
@$core.Deprecated('Use newSigningAccountDescriptor instead')
const NewSigningAccount$json = const {
  '1': 'NewSigningAccount',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'rpc_http_address', '3': 2, '4': 1, '5': 9, '10': 'rpcHttpAddress'},
    const {'1': 'rpc_ws_address', '3': 3, '4': 1, '5': 9, '10': 'rpcWsAddress'},
    const {'1': 'seed_phrase', '3': 4, '4': 1, '5': 9, '10': 'seedPhrase'},
    const {'1': 'passphrase', '3': 5, '4': 1, '5': 9, '10': 'passphrase'},
    const {'1': 'derivation_path', '3': 6, '4': 1, '5': 9, '10': 'derivationPath'},
    const {'1': 'encryption_password', '3': 7, '4': 1, '5': 9, '10': 'encryptionPassword'},
  ],
};

/// Descriptor for `NewSigningAccount`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newSigningAccountDescriptor = $convert.base64Decode('ChFOZXdTaWduaW5nQWNjb3VudBISCgRuYW1lGAEgASgJUgRuYW1lEigKEHJwY19odHRwX2FkZHJlc3MYAiABKAlSDnJwY0h0dHBBZGRyZXNzEiQKDnJwY193c19hZGRyZXNzGAMgASgJUgxycGNXc0FkZHJlc3MSHwoLc2VlZF9waHJhc2UYBCABKAlSCnNlZWRQaHJhc2USHgoKcGFzc3BocmFzZRgFIAEoCVIKcGFzc3BocmFzZRInCg9kZXJpdmF0aW9uX3BhdGgYBiABKAlSDmRlcml2YXRpb25QYXRoEi8KE2VuY3J5cHRpb25fcGFzc3dvcmQYByABKAlSEmVuY3J5cHRpb25QYXNzd29yZA==');
@$core.Deprecated('Use newAccountTransactionDescriptor instead')
const NewAccountTransaction$json = const {
  '1': 'NewAccountTransaction',
  '2': const [
    const {'1': 'destination', '3': 1, '4': 1, '5': 9, '10': 'destination'},
    const {'1': 'amount', '3': 2, '4': 1, '5': 4, '10': 'amount'},
    const {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `NewAccountTransaction`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List newAccountTransactionDescriptor = $convert.base64Decode('ChVOZXdBY2NvdW50VHJhbnNhY3Rpb24SIAoLZGVzdGluYXRpb24YASABKAlSC2Rlc3RpbmF0aW9uEhYKBmFtb3VudBgCIAEoBFIGYW1vdW50EhoKCHBhc3N3b3JkGAMgASgJUghwYXNzd29yZA==');
