///
//  Generated code. Do not modify.
//  source: proto/vaullet.ffi.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Error extends $pb.ProtobufEnum {
  static const Error NONE = Error._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NONE');
  static const Error NULL_POINTER_INPUT = Error._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NULL_POINTER_INPUT');
  static const Error STR_CONVERT_ERROR = Error._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'STR_CONVERT_ERROR');
  static const Error PATH_PARSE_ERROR = Error._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PATH_PARSE_ERROR');
  static const Error DOC_DIR_ACCESS_ERROR = Error._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DOC_DIR_ACCESS_ERROR');
  static const Error CACHE_DIR_ACCESS_ERROR = Error._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CACHE_DIR_ACCESS_ERROR');
  static const Error SPAWN_BLOCKING_ERROR = Error._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SPAWN_BLOCKING_ERROR');
  static const Error DATA_DB_OPEN_ERROR = Error._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DATA_DB_OPEN_ERROR');
  static const Error CACHE_DB_OPEN_ERROR = Error._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CACHE_DB_OPEN_ERROR');
  static const Error JOIN_ERROR = Error._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'JOIN_ERROR');
  static const Error CONNECTION_ERROR = Error._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CONNECTION_ERROR');
  static const Error INCORRECT_PUBLIC_KEY_FORMAT = Error._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_PUBLIC_KEY_FORMAT');
  static const Error INCORRECT_KEYPAIR_FORMAT = Error._(12, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_KEYPAIR_FORMAT');
  static const Error INCORRECT_ENCODING_FORMAT = Error._(13, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_ENCODING_FORMAT');
  static const Error INCORRECT_DERIVATION_PATH_FORMAT = Error._(14, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_DERIVATION_PATH_FORMAT');
  static const Error UNABLE_TO_DERIVE_KEY = Error._(15, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'UNABLE_TO_DERIVE_KEY');
  static const Error HASH_OUTPUT_BUFFER_ERROR = Error._(16, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'HASH_OUTPUT_BUFFER_ERROR');
  static const Error ACCOUNT_CAN_NOT_SIGN = Error._(17, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ACCOUNT_CAN_NOT_SIGN');
  static const Error NO_ACTIVE_ACCOUNT = Error._(18, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'NO_ACTIVE_ACCOUNT');
  static const Error INCORRECT_TRANSACTION_STATUS = Error._(19, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_TRANSACTION_STATUS');
  static const Error DECRYPTION_ERROR = Error._(20, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'DECRYPTION_ERROR');
  static const Error SIGNING_ERROR = Error._(21, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SIGNING_ERROR');
  static const Error INCORRECT_DESTINATION_ADDRESS_FORMAT = Error._(22, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_DESTINATION_ADDRESS_FORMAT');
  static const Error TRANSACTION_SEND_ERROR = Error._(23, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'TRANSACTION_SEND_ERROR');
  static const Error INCORRECT_SIGNATURE_FORMAT = Error._(24, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'INCORRECT_SIGNATURE_FORMAT');
  static const Error OPEN_DATA_TREE_ERROR = Error._(25, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OPEN_DATA_TREE_ERROR');
  static const Error UNKNOWN_ERROR = Error._(26, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'UNKNOWN_ERROR');

  static const $core.List<Error> values = <Error> [
    NONE,
    NULL_POINTER_INPUT,
    STR_CONVERT_ERROR,
    PATH_PARSE_ERROR,
    DOC_DIR_ACCESS_ERROR,
    CACHE_DIR_ACCESS_ERROR,
    SPAWN_BLOCKING_ERROR,
    DATA_DB_OPEN_ERROR,
    CACHE_DB_OPEN_ERROR,
    JOIN_ERROR,
    CONNECTION_ERROR,
    INCORRECT_PUBLIC_KEY_FORMAT,
    INCORRECT_KEYPAIR_FORMAT,
    INCORRECT_ENCODING_FORMAT,
    INCORRECT_DERIVATION_PATH_FORMAT,
    UNABLE_TO_DERIVE_KEY,
    HASH_OUTPUT_BUFFER_ERROR,
    ACCOUNT_CAN_NOT_SIGN,
    NO_ACTIVE_ACCOUNT,
    INCORRECT_TRANSACTION_STATUS,
    DECRYPTION_ERROR,
    SIGNING_ERROR,
    INCORRECT_DESTINATION_ADDRESS_FORMAT,
    TRANSACTION_SEND_ERROR,
    INCORRECT_SIGNATURE_FORMAT,
    OPEN_DATA_TREE_ERROR,
    UNKNOWN_ERROR,
  ];

  static final $core.Map<$core.int, Error> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Error? valueOf($core.int value) => _byValue[value];

  const Error._($core.int v, $core.String n) : super(v, n);
}

