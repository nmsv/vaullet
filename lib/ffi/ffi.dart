import 'dart:async';
import 'dart:typed_data';
import 'dart:ffi';
import 'dart:isolate';
import 'package:ffi/ffi.dart';
import 'package:isolate/ports.dart';

import 'package:vaullet/ffi/ffi.g.dart' as native;

import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart';

import 'package:path_provider/path_provider.dart';

class Ffi {
  static final Ffi _instance = Ffi._setup();
  late final Stream stateStream;

  Ffi._setup() {
    native.store_dart_post_cobject(NativeApi.postCObject);
    stateStream = _stateSubscribe();
  }

  factory Ffi() => _instance;

  Future<void> appInit() async {
    final docDir = await getApplicationDocumentsDirectory();
    final cacheDir = await getTemporaryDirectory();
    final docPointer = docDir.absolute.path.toNativeUtf8();
    final cachePointer = cacheDir.absolute.path.toNativeUtf8();
    final completer = Completer<int>();
    final sendPort = singleCompletePort(completer);
    final res = Error.valueOf(native.app_init(
      sendPort.nativePort,
      docPointer,
      cachePointer,
    ));
    malloc.free(docPointer);
    malloc.free(cachePointer);
    if (res != Error.NONE) _throwError(res);

    final resFuture = Error.valueOf(await completer.future);

    if (resFuture != Error.NONE) _throwError(resFuture);
  }

  Stream _stateSubscribe() {
    final rx = ReceivePort();
    native.get_state_subscribe(
      rx.sendPort.nativePort,
    );
    return rx
      .map((buf) => State.fromBuffer(buf))
      .asBroadcastStream();
  }

  Stream getLogStream() {
    final rx = ReceivePort();
    native.log_init(
      rx.sendPort.nativePort,
    );
    return rx;
  }

  Future<void> addWatchAccount(NewWatchAccount account) async {
    final completer = Completer<int>();
    final sendPort = singleCompletePort(completer);
    
    final buf = account.writeToBuffer();
    var ptr = malloc<Uint8>(buf.length);
    ptr.asTypedList(buf.length).setAll(0, buf);
    
    final res = Error.valueOf(native.add_watch_account(
      ptr,
      buf.length,
      sendPort.nativePort,
    ));
    malloc.free(ptr);
    if (res != Error.NONE) _throwError(res);

    final resFuture = Error.valueOf(await completer.future);

    if (resFuture != Error.NONE) _throwError(resFuture);
  }

  Future<void> addSigningAccount(NewSigningAccount account) async {
    final completer = Completer<int>();
    final sendPort = singleCompletePort(completer);
    
    final buf = account.writeToBuffer();
    var ptr = malloc<Uint8>(buf.length);
    ptr.asTypedList(buf.length).setAll(0, buf);
    
    final res = Error.valueOf(native.add_signing_account(
      ptr,
      buf.length,
      sendPort.nativePort,
    ));
    malloc.free(ptr);
    if (res != Error.NONE) _throwError(res);

    final resFuture = Error.valueOf(await completer.future);

    if (resFuture != Error.NONE) _throwError(resFuture);
  }
  
  Future<void> sendTransaction(NewAccountTransaction transaction) async {
    final completer = Completer<int>();
    final sendPort = singleCompletePort(completer);
    
    final buf = transaction.writeToBuffer();
    var ptr = malloc<Uint8>(buf.length);
    ptr.asTypedList(buf.length).setAll(0, buf);
    
    final res = Error.valueOf(native.send_from_active_account(
      ptr,
      buf.length,
      sendPort.nativePort,
    ));
    malloc.free(ptr);
    if (res != Error.NONE) _throwError(res);

    final resFuture = Error.valueOf(await completer.future);

    if (resFuture != Error.NONE) _throwError(resFuture);
  }

  void setActiveAccount(int i) {
    native.set_active_account(i);
  }

  Future<State> getState() async {
    final completer = Completer<List<int>>();
    final sendPort = singleCompletePort(completer);
    
    native.get_state(
      sendPort.nativePort,
    );

    final buf = await completer.future;
    return State.fromBuffer(buf);
  }

  void cameraSubmitImage({
    required Uint8List image,
    required int width,
    required int height,
  }) {
    var ptr = malloc<Uint8>(image.length);
    ptr.asTypedList(image.length).setAll(0, image);
    
    final res = Error.valueOf(native.camera_submit_image(
      ptr,
      image.length,
      width,
      height,
    ));
    malloc.free(ptr);
    if (res != Error.NONE) _throwError(res);
  }
 
  Future<String> cameraGetResult() async {
    final completer = Completer<String>();
    final sendPort = singleCompletePort(completer);
    
    native.camera_get_result(
      sendPort.nativePort,
    );

    return await completer.future;
  }

  void _throwError(Error? errCode) {
    throw errCode ?? Error.UNKNOWN_ERROR;
  }
}
