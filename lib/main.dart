import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vaullet/pages/common_tabs.dart';
import 'package:vaullet/ffi/ffi.dart';

final navigatorKey = GlobalKey<NavigatorState>();

Future<void> init() async {
  Ffi().getLogStream()
    .listen((s) => print(s));
  
  Ffi().appInit();
}

class CommonScrollBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}

void main() {
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    systemNavigationBarColor: Color(0xff1A1A1A),
  ));

  WidgetsFlutterBinding.ensureInitialized();
  init().then((exitCode) => runApp(const VaulletApp()));
}

class VaulletApp extends StatelessWidget {
  const VaulletApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vaullet',
      navigatorKey: navigatorKey,
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
      ],
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: CommonScrollBehavior(),
          child: child ?? Container(),
        );
      },
      theme: ThemeData(
        primarySwatch: Colors.teal,
	    brightness: Brightness.dark,
	    primaryColorLight: const Color(0xff505050),
        primaryColorDark: const Color(0xff303030),
        backgroundColor: const Color(0xff1A1A1A),
        scaffoldBackgroundColor: const Color(0xff1A1A1A),
        snackBarTheme: const SnackBarThemeData(
          behavior: SnackBarBehavior.fixed,
        ),
      ),
      home: const CommonTabsPage(),
    );
  }
}
