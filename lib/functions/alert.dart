import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart';

Future<void> errorAlert({
  required String actionName,
  required Error? error,
  required BuildContext context
}) async {
  final snackBar = SnackBar(
    backgroundColor: Colors.red,
    content: ListTile(
      leading: const Icon(Icons.error_outline_outlined),
      title: Text('$actionName ${AppLocalizations.of(context)!.alertResultFailed}'),
      subtitle: Text('Error: ${_mapErrorToString(error ?? Error.UNKNOWN_ERROR, context)}'),
    ),
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

Future<void> errorStringAlert({
  required String actionName,
  required String? error,
  required BuildContext context
}) async {
  final snackBar = SnackBar(
    backgroundColor: Colors.red,
    content: ListTile(
      leading: const Icon(Icons.error_outline_outlined),
      title: Text('$actionName ${AppLocalizations.of(context)!.alertResultFailed}'),
      subtitle: error != null 
        ? Text('Error: $error')
        : null,
    ),
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

Future<void> successAlert({
  required String actionName,
  required BuildContext context
}) async {
  final snackBar = SnackBar(
    backgroundColor: Colors.green,
    content: ListTile(
      leading: const Icon(Icons.check_circle_outlined),
      title: Text('$actionName ${AppLocalizations.of(context)!.alertResultSuccessful}'),
    ),
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

String _mapErrorToString(Error error, BuildContext context) {
  switch(error) { 

    case Error.NONE: {
        return AppLocalizations.of(context)!.ffiErrorNone;
    }

    // Common
    case Error.NULL_POINTER_INPUT: {
        return AppLocalizations.of(context)!.ffiErrorNullPointerInput;
    }


    case Error.STR_CONVERT_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorStrConvertError;
    }


    case Error.PATH_PARSE_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorPathParseError;
    }


    case Error.DOC_DIR_ACCESS_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorDocDirAccessError;
    }


    case Error.CACHE_DIR_ACCESS_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorCacheDirAccessError;
    }


    case Error.SPAWN_BLOCKING_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorSpawnBlockingError;
    }


    case Error.DATA_DB_OPEN_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorDataDbOpenError;
    }


    case Error.CACHE_DB_OPEN_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorCacheDbOpenError;
    }


    case Error.JOIN_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorJoinError;
    }


    case Error.CONNECTION_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorConnectionError;
    }



    // Account
    case Error.INCORRECT_PUBLIC_KEY_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectPublicKeyFormat;
    }


    case Error.INCORRECT_KEYPAIR_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectKeypairFormat;
    }


    case Error.INCORRECT_ENCODING_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectEncodingFormat;
    }


    case Error.INCORRECT_DERIVATION_PATH_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectDerivationPathFormat;
    }


    case Error.UNABLE_TO_DERIVE_KEY: {
        return AppLocalizations.of(context)!.ffiErrorUnableToDeriveKey;
    }


    case Error.HASH_OUTPUT_BUFFER_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorHashOutputBufferError;
    }


    case Error.ACCOUNT_CAN_NOT_SIGN: {
        return AppLocalizations.of(context)!.ffiErrorAccountCanNotSign;
    }


    case Error.NO_ACTIVE_ACCOUNT: {
        return AppLocalizations.of(context)!.ffiErrorNoActiveAccount;
    }



    // Transaction
    case Error.INCORRECT_TRANSACTION_STATUS: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectTransactionStatus;
    }


    case Error.DECRYPTION_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorDecryptionError;
    }


    case Error.SIGNING_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorSigningError;
    }


    case Error.INCORRECT_DESTINATION_ADDRESS_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectDestinationAddressFormat;
    }


    case Error.TRANSACTION_SEND_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorTransactionSendError;
    }


    case Error.INCORRECT_SIGNATURE_FORMAT: {
        return AppLocalizations.of(context)!.ffiErrorIncorrectSignatureFormat;
    }



    // Database
    case Error.OPEN_DATA_TREE_ERROR: {
        return AppLocalizations.of(context)!.ffiErrorOpenDataTreeError;
    }

    default: { 
      return AppLocalizations.of(context)!.ffiErrorUnknownError;
    }
  }
}
