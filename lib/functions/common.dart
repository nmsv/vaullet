import 'dart:core';
import 'dart:math' as math;

String displayAmount(int value, int decimalAmount, {bool signed = false}) {
  
  final whole = value.abs() ~/ math.pow(10, decimalAmount);
  final decimal = value.abs() % math.pow(10, decimalAmount);
  final sign = value < 0 
    ? '-'
    : signed
      ? value > 0
        ? '+' : ''
      : '';

  String decimalString = decimal
    .toString()
    .padLeft(9, '0')
    .replaceAll(RegExp(r'0+$'), '');

  return '$sign $whole.${decimalString.isEmpty ? 0 : decimalString}';
}

int currencyFromString(String string, int decimalAmount) {
  if (string.contains(RegExp(r'[^0-9.]'))) {
    throw 1;
  }

  final amountSplit = string.split('.');

  if (amountSplit.length > 2) {
    throw 2;
  }

  final wholePart = amountSplit[0].isEmpty ? '0' : amountSplit[0];

  final whole = int.parse(wholePart) * math.pow(10, decimalAmount).toInt();

  if (amountSplit.length == 1 || amountSplit[1].isEmpty) {
    return whole.abs();
  }

  if (amountSplit[1].runes.length > decimalAmount) {
    throw 3;
  }

  return whole.abs() + int.parse(amountSplit[1].padRight(decimalAmount, '0')).abs();
}

String displaySolAmount(lamports, {bool signed = false}) {
  final value = lamports.runtimeType != int ? lamports.toInt() : lamports;
  return displayAmount(value, 9, signed: signed);
}

int solFromString(String string) {
  return currencyFromString(string, 9);
}
