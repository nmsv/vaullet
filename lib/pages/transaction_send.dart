import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:camera/camera.dart';
import 'package:fixnum/fixnum.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart' as ffi;
import 'package:vaullet/pages/camera_scan.dart';
import 'package:vaullet/widgets/field_padding.dart';
import 'package:vaullet/widgets/list_spaces.dart';
import 'package:vaullet/functions/common.dart';
import 'package:vaullet/functions/alert.dart';
import 'package:vaullet/main.dart';

class TransactionSendPage extends StatefulWidget {
  const TransactionSendPage({Key? key}) : super(key: key);

  @override
  State<TransactionSendPage> createState() => _TransactionSendPageState();
}

class _TransactionSendPageState extends State<TransactionSendPage> {

  final TextEditingController _addressCtrl = TextEditingController();
  final TextEditingController _amountCtrl = TextEditingController();
  final TextEditingController _passwordCtrl = TextEditingController();

  bool isLoading = false;

  void _sendTransaction() async {
    setState(() => isLoading = true);
    final amount = solFromString(_amountCtrl.text);
    
    final ffi.NewAccountTransaction transaction = ffi.NewAccountTransaction(
      destination: _addressCtrl.text,
      amount: Int64(amount),
      password: _passwordCtrl.text,
    );

    bool isSuccess = false;

    try {
      await Ffi().sendTransaction(transaction);
      isSuccess = true;
    } catch (e) {
      final context = navigatorKey.currentContext!;
      final ffi.Error error = e is ffi.Error
        ? e
        : ffi.Error.UNKNOWN_ERROR;
      errorAlert(
        error: error,
        context: context,
        actionName: AppLocalizations.of(context)!.alertActionTransaction,
      );
    }

    try {
      if (isSuccess) {
        final context = navigatorKey.currentContext!;
        successAlert(
          context: context,
          actionName: AppLocalizations.of(context)!.alertActionTransaction,
        );
      }
    } catch (_) {}


    try {
      setState(() => isLoading = false);
      if (isSuccess) {
        Navigator.of(context).pop();
      }
    } catch (_) {}
  }

  _toCamera(BuildContext context) async {
    final cameras = await availableCameras();
    if (cameras.isEmpty) {
      errorStringAlert(
        error: AppLocalizations.of(context)!.errorNoCamera,
        context: context,
        actionName: AppLocalizations.of(context)!.alertActionCamera,
      );

      return;
    }
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => CameraScanPage(
        camera: cameras[0],
        onDetect: (text) {
          try {
            final schemeSplit = text.split(':');
            
            if (schemeSplit[0] != 'solana') {
              throw 1;
            }

            final addressSplit = schemeSplit[1].split('?');

            _addressCtrl.text = addressSplit[0];

            if (addressSplit.length > 1) {
              final req = Uri.parse('?${addressSplit[1]}');

              _amountCtrl.text = req.queryParameters['amount'] ?? '';
            }
          } catch (e) {
            _addressCtrl.text = text;
          }
        },
      ))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.titleNewTransaction),
      ),
      body: ListView(
        children: [
          const ListSpaceTop(),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _addressCtrl,
              obscureText: false,
              keyboardType: TextInputType.visiblePassword,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'[1-9A-HJ-NP-Za-km-z]')),
              ],
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldAddress,
              ),
            ),
          ),
          FieldPadding(
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    maxLines: 1,
                    controller: _amountCtrl,
                    obscureText: false,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      TextInputFormatter.withFunction((prev, next) {
                        try {
                          solFromString(next.text);
                          return next;
                        } catch (e) {
                          return prev;
                        }
                      }),
                    ],
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      labelText: AppLocalizations.of(context)!.fieldAmount,
                    ),
                  ),
                ),
                Container( width: 8 ),
                IconButton(
                  icon: const Icon(Icons.qr_code_2_rounded),
                  onPressed: () => _toCamera(context),
                ),
              ],
            ),
          ),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _passwordCtrl,
              obscureText: true,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldSigningPassword,
              ),
            ),
          ),
          const ListSpaceBottom(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => isLoading ? null : _sendTransaction(),
        child: isLoading 
          ? CircularProgressIndicator(
            backgroundColor: Theme.of(context).backgroundColor,
            color: Colors.purpleAccent,
          )
          : const Icon(Icons.check),
      ),
    );
  }
}
