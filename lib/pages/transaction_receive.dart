import 'dart:async';
import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/widgets/field_padding.dart';
import 'package:vaullet/widgets/list_spaces.dart';
import 'package:vaullet/functions/common.dart';

class TransactionReceivePage extends StatefulWidget {
  const TransactionReceivePage({Key? key}) : super(key: key);

  @override
  State<TransactionReceivePage> createState() => _TransactionReceivePageState();
}

class _TransactionReceivePageState extends State<TransactionReceivePage> {

  final TextEditingController _addressCtrl = TextEditingController();
  final TextEditingController _amountCtrl = TextEditingController();
  late final StreamSubscription _sub;

  String qrData = '';

  void stateFromGlobal(state) => setState(() {
    _addressCtrl.text = state.accounts[state.activeAccount].address;
  });

  _setQrData() {
    setState(() {
      final amount = solFromString(_amountCtrl.text);
      
      qrData = 'solana:${_addressCtrl.text}' 
        + (amount != 0 ? '?amount=$amount' : '');
    });
  }

  @override
  void initState() {
    super.initState();

    Ffi().getState()
      .then((state) {
        stateFromGlobal(state);
        _setQrData();
      });

    _sub = Ffi().stateStream
      .distinct((prev, next) =>
        prev.accounts[prev.activeAccount] == next.accounts[next.activeAccount]
      )
      .listen(stateFromGlobal);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.titleReceiveTransaction),
      ),
      body: ListView(
        children: [
          const ListSpaceTop(),
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) => Center(
              child: QrImage(
                backgroundColor: Colors.white,
                data: qrData,
                version: QrVersions.auto,
                size: math.min(
                  constraints.maxWidth, 
                  MediaQuery.of(context).size.height
                    - Scaffold.of(context).appBarMaxHeight!,
                ) - 16,
              ),
            ),
          ),
          const ListSpaceTop(),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              obscureText: false,
              readOnly: true,
              controller: _addressCtrl,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldAddress,
              ),
            ),
          ),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _amountCtrl,
              obscureText: false,
              keyboardType: TextInputType.number,
              inputFormatters: [
                TextInputFormatter.withFunction((prev, next) {
                  try {
                    solFromString(next.text);
                    return next;
                  } catch (e) {
                    return prev;
                  }
                }),
              ],
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldAmount,
              ),
              onChanged: (text) => _setQrData(),
            ),
          ),
          const ListSpaceBottom(),
        ],
      ),
    );
  }
  
  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
