import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart' as ffi;
import 'package:vaullet/widgets/field_padding.dart';
import 'package:vaullet/widgets/list_spaces.dart';
import 'package:vaullet/functions/alert.dart';
import 'package:vaullet/main.dart';

class AccountAddPage extends StatefulWidget {
  const AccountAddPage({Key? key}) : super(key: key);

  @override
  State<AccountAddPage> createState() => _AccountAddPageState();
}

class _AccountAddPageState extends State<AccountAddPage> {

  final TextEditingController _nameCtrl = TextEditingController();
  final TextEditingController _rpcHttpAddressCtrl = TextEditingController();
  final TextEditingController _rpcWsAddressCtrl = TextEditingController();
  final TextEditingController _addressCtrl = TextEditingController();
  final TextEditingController _seedPhraseCtrl = TextEditingController();
  final TextEditingController _passphraseCtrl = TextEditingController();
  final TextEditingController _derivPathCtrl = TextEditingController();
  final TextEditingController _encPassCtrl = TextEditingController();

  bool isWatch = false;
  bool isLoading = false;

  void _addAccount() async {
    
    setState(() => isLoading = true);
    
    bool isSuccess = false;
    
    try {
      if (isWatch) {
        final ffi.NewWatchAccount account = ffi.NewWatchAccount(
          name: _nameCtrl.text,
          rpcHttpAddress: _rpcHttpAddressCtrl.text,
          rpcWsAddress: _rpcWsAddressCtrl.text,
          address: _addressCtrl.text,
        );

        Ffi().addWatchAccount(account);
      } else {
        final ffi.NewSigningAccount account = ffi.NewSigningAccount(
          name: _nameCtrl.text,
          rpcHttpAddress: _rpcHttpAddressCtrl.text,
          rpcWsAddress: _rpcWsAddressCtrl.text,
          seedPhrase: _seedPhraseCtrl.text,
          passphrase: _passphraseCtrl.text,
          derivationPath: _derivPathCtrl.text,
          encryptionPassword: _encPassCtrl.text,
        );

        Ffi().addSigningAccount(account);
      }
      isSuccess = true;
    } catch (e) {
      final context = navigatorKey.currentContext!;
      final ffi.Error error = e is ffi.Error
        ? e
        : ffi.Error.UNKNOWN_ERROR;
      errorAlert(
        error: error,
        context: context,
        actionName: AppLocalizations.of(context)!.alertActionAccountCreation,
      );
    }

    try {
      if (isSuccess) {
        final context = navigatorKey.currentContext!;
        successAlert(
          context: context,
          actionName: AppLocalizations.of(context)!.alertActionAccountCreation,
        );
      }
    } catch (_) {}

    try {
      setState(() => isLoading = false);
      if (isSuccess) {
        Navigator.of(context).pop();
      }
    } catch (_) {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.titleAddAccount),
      ),
      body: ListView(
        children: [
          const ListSpaceTop(),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _nameCtrl,
              obscureText: false,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldName,
              ),
            ),
          ),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _rpcHttpAddressCtrl,
              obscureText: false,
              keyboardType: TextInputType.url,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldRpcHttpAddress,
              ),
            ),
          ),
          FieldPadding(
            child: TextField(
              maxLines: 1,
              controller: _rpcWsAddressCtrl,
              obscureText: false,
              keyboardType: TextInputType.url,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: AppLocalizations.of(context)!.fieldRpcWebsocketAddress,
              ),
            ),
          ),
          ListTile(
            title: Text(AppLocalizations.of(context)!.fieldWatchOnly),
            trailing: Switch(
              value: isWatch,
              onChanged: (iw) => setState(() => isWatch = iw),
            ),
          ),
          Visibility(
            visible: isWatch,
            child: FieldPadding(
              child: TextField(
                maxLines: 1,
                controller: _addressCtrl,
                obscureText: false,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.fieldAddress,
                ),
              ),
            ),
          ),
          Visibility(
            visible: !isWatch,
            child: FieldPadding(
              child: TextField(
                minLines: 3,
                maxLines: 3,
                controller: _seedPhraseCtrl,
                obscureText: false,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.fieldSeedPhrase,
                ),
              ),
            ),
          ),
          Visibility(
            visible: !isWatch,
            child: FieldPadding(
              child: TextField(
                maxLines: 1,
                controller: _passphraseCtrl,
                obscureText: true,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.fieldPassphrase,
                ),
              ),
            ),
          ),
          Visibility(
            visible: !isWatch,
            child: FieldPadding(
              child: TextField(
                maxLines: 1,
                controller: _derivPathCtrl,
                obscureText: false,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.fieldDerivationPath,
                ),
              ),
            ),
          ),
          Visibility(
            visible: !isWatch,
            child: FieldPadding(
              child: TextField(
                maxLines: 1,
                controller: _encPassCtrl,
                obscureText: true,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.fieldSigningPassword,
                ),
              ),
            ),
          ),
          const ListSpaceBottom(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => isLoading ? null : _addAccount(),
        child: isLoading 
          ? CircularProgressIndicator(
            backgroundColor: Theme.of(context).backgroundColor,
            color: Colors.purpleAccent,
          )
          : const Icon(Icons.check),
      ),
    );
  }
}
