import 'package:flutter/material.dart';
import 'package:vaullet/widgets/list_spaces.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: const [
        ListSpaceTop(),
        ListSpaceBottom(),
      ],
    );
  }
}
