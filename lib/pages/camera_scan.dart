import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:camera/camera.dart';
import 'package:vaullet/ffi/ffi.dart';

class CameraScanPage extends StatefulWidget {
  const CameraScanPage({
    Key? key,
    required this.onDetect,
    required this.camera,
  }) : super(key: key);

  final Function(String) onDetect;
  final CameraDescription camera;
  
  @override
  _CameraScanPageState createState() => _CameraScanPageState();
}

class _CameraScanPageState extends State<CameraScanPage> {
  late CameraController _controller;
  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.low,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.yuv420,
    );
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
      _controller.startImageStream((image) {
        final width = image.planes[0].width ?? image.planes[0].bytesPerRow / image.planes[0].bytesPerPixel!;
        final height = image.planes[0].height ?? image.planes[0].bytes.length / image.planes[0].bytesPerRow;
        Ffi().cameraSubmitImage(
          image: image.planes[0].bytes,
          width: width.toInt(),
          height: height.toInt(),
        );
      });

      _subscription = Ffi().cameraGetResult().asStream().listen((text) {
        widget.onDetect(text);
        Navigator.of(context).pop();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.titleScanCode),
      ),
      body: CameraPreview(_controller),
      /*floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addAccount();
          Navigator.of(context).pop();
        },
        tooltip: 'Send',
        child: const Icon(Icons.check),
      ),*/
    );
  }
  
  @override
  void dispose() {
    _controller.dispose();
    _subscription.cancel();
    super.dispose();
  }
}
