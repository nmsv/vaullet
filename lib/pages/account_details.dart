import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart' as ffi;
import 'package:vaullet/widgets/transaction_card.dart';
import 'package:vaullet/widgets/list_spaces.dart';
import 'package:vaullet/functions/common.dart';

class AccountDetailsPage extends StatefulWidget {
  const AccountDetailsPage({Key? key}) : super(key: key);

  @override
  State<AccountDetailsPage> createState() => _AccountDetailsPageState();
}

class _AccountDetailsPageState extends State<AccountDetailsPage> {

  late final StreamSubscription _sub;
  int balance = 0;
  List<ffi.Transaction> transactions = List.empty();
  bool loading = false;

  void stateFromGlobal(state) { 
    try {
      setState(() {
        balance = state.accounts[state.activeAccount].balance.toInt();
        transactions = state.transactions;
        loading = state.transactionLoading;
      });
    } catch (_) {}
  }

  @override
  void initState() {
    super.initState();

    Ffi().getState()
      .then(stateFromGlobal);

    _sub = Ffi().stateStream 
      .distinct((prev, next) => 
        prev.accounts[prev.activeAccount].balance 
          == next.accounts[next.activeAccount].balance
        && prev.transactions == next.transactions
        && prev.transactionLoading == next.transactionLoading
      )
      .listen(stateFromGlobal);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          const ListSpaceTop(),
          Text(
            '${AppLocalizations.of(context)!.balance}:',
          ),
          Text(
            displaySolAmount(balance),
            style: Theme.of(context).textTheme.headline4,
          ),
          const ListSpaceTop(),
          AnimatedContainer(
            child: const LinearProgressIndicator(),
            duration: const Duration(milliseconds: 100),
            height: loading ? 2 : 0,
          ),
          Expanded(
            child: ListView.separated(
              itemCount: transactions.length + 1,
              itemBuilder: (BuildContext context, int index) {
                if (index == transactions.length) {
                  return const ListSpaceBottom();
                }
                return TransactionCard(
                  transaction: transactions[index],
                );
              },
              separatorBuilder: (BuildContext context, int index) => const Divider(),
            ),
          ),
        ],
      ),
    );
  }
  
  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
