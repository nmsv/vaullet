import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/pages/accounts_list.dart';
import 'package:vaullet/pages/account_details.dart';
import 'package:vaullet/pages/settings.dart';
import 'package:vaullet/pages/account_add.dart';
import 'package:vaullet/pages/transaction_send.dart';
import 'package:vaullet/pages/transaction_receive.dart';
import 'package:vaullet/widgets/floating_menu.dart';

class CommonTabsPage extends StatefulWidget {
  const CommonTabsPage({Key? key}) : super(key: key);

  @override
  State<CommonTabsPage> createState() => CommonTabsPageState();
}

class CommonTabsPageState extends State<CommonTabsPage> with SingleTickerProviderStateMixin {

  late final StreamSubscription _sub;
  late final TabController _tabController;
  late final AnimationController _buttonController;
  bool hasActiveAccount = false;
  bool accountCanSign = false;

  void _buttonAnimation() {
    final offset = _tabController.offset % 1;

    List<int> buttonEnabledOn = [0];

    if (hasActiveAccount) {
      buttonEnabledOn.add(1);
    }
    
    if (
      (_buttonController.isCompleted 
        || _buttonController.status
          == AnimationStatus.forward)
      && offset != 0
    ) {
      _buttonController.reverse();
    }

    if (
      (_buttonController.isDismissed 
        || _buttonController.status
          == AnimationStatus.reverse)
      && offset == 0
      && buttonEnabledOn.contains(_tabController.index)
    ) {
      _buttonController.forward();
    }
  }

  void stateFromGlobal(state) => setState(() {
    hasActiveAccount = state.activeAccount < state.accounts.length;
    accountCanSign = hasActiveAccount 
      ? state.accounts[state.activeAccount].canSign
      : false;
  });

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);

    _buttonController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );

    Ffi().getState()
      .then(stateFromGlobal);

    _sub = Ffi().stateStream 
      .distinct((prev, next) {
        final bool? prevCanSign = prev.activeAccount < prev.accounts.length 
          ? prev.accounts[prev.activeAccount].canSign
          : null;
        final bool? nextCanSign = next.activeAccount < next.accounts.length 
          ? next.accounts[next.activeAccount].canSign
          : null;
        return prev.activeAccount 
          == next.activeAccount
        && prev.accounts.length == next.accounts.length
        && prevCanSign == nextCanSign;
      })
      .listen(stateFromGlobal);

    _buttonAnimation();

    _tabController.animation!.addListener(_buttonAnimation);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> tabs = [
      const Tab(
        icon: Icon(Icons.account_balance_wallet_outlined)
      ),
      const Tab(
        icon: Icon(Icons.settings_applications_outlined)
      ),
    ];
    
    if (hasActiveAccount) {
      tabs.insert(1,
        const Tab(
          icon: Icon(Icons.swap_vertical_circle_outlined)
        ),
      );
    }
   
    List<Widget> views = [
      const AccountsListPage(),
      const SettingsPage(),
    ];
    
    if (hasActiveAccount) {
      views.insert(1, const AccountDetailsPage());
    }

    List<Widget> buttons = [];

    if (hasActiveAccount) {
      buttons.add(
        FloatingActionButton(
          child: const Icon(Icons.download),
          onPressed: () => Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => const TransactionReceivePage()),
          ),
        )
      );

      if (accountCanSign) {
        buttons.insert(0,
          FloatingActionButton(
            child: const Icon(Icons.upload),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => const TransactionSendPage()),
            ),
          )
        );
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Vaullet'),
        bottom: tabs.length > 1 
          ? TabBar(
            controller: _tabController,
            tabs: tabs,
          )
          : null,
      ),
      body: TabBarView(
        controller: _tabController,
        children: views,
      ),
      floatingActionButton: AnimatedBuilder(
        animation: _buttonController,
        builder: (BuildContext context, Widget? child) {
          return Transform.scale(
            alignment: FractionalOffset.bottomRight,
            scale: _buttonController.value,
            child: _tabController.index == 0
              ? FloatingActionButton(
                child: const Icon(Icons.add),
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const AccountAddPage())
                ),
              )
              : hasActiveAccount && _tabController.index == 1
                ? FloatingMenu(
                  closeController: _tabController.animation!,
                  buttons: buttons,
                )
                : null,
          );
        }
      ),
    );
  }
  
  @override
  void dispose() {
    _sub.cancel();
    _tabController.dispose();
    super.dispose();
  }
}
