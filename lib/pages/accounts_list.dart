import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/widgets/account_card.dart';
import 'package:vaullet/widgets/list_spaces.dart';

class AccountsListPage extends StatefulWidget {
  const AccountsListPage({Key? key}) : super(key: key);

  @override
  State<AccountsListPage> createState() => _AccountsListPageState();
}

class _AccountsListPageState extends State<AccountsListPage> {

  late final StreamSubscription _sub;
  int accounts = 0;

  void stateFromGlobal(state) => setState(() {
    accounts = state.accounts.length;
  });

  @override
  void initState() {
    super.initState();

    Ffi()
      .getState()
      .then(stateFromGlobal);

    _sub = Ffi()
      .stateStream 
      .distinct((prev, next) =>
        prev.accounts.length == next.accounts.length
      )
      .listen(stateFromGlobal);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: accounts + 2,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return const ListSpaceTop();
          }
          if (index == accounts + 1) {
            return const ListSpaceBottom();
          }
          return AccountCard(
            index: index - 1,
          );
        }
    );
  }
  
  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
