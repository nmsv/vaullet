import 'dart:math';
import 'package:flutter/material.dart';

class FloatingMenu extends StatefulWidget {
  final List<Widget> buttons;
  final Animation closeController;

  const FloatingMenu({
    Key? key,
    required this.buttons,
    required this.closeController
  }) : super(key: key);
  
  @override
  State createState() => _FloatingMenuState();
}

class _FloatingMenuState extends State<FloatingMenu> with SingleTickerProviderStateMixin {
  late final AnimationController _controller;

  void _close() {
    if (_controller.isCompleted) {
      _controller.reverse();
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: widget.buttons.length * 100),
    );

    widget.closeController.addListener(_close);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: List<Widget>.generate(widget.buttons.length, (int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: ScaleTransition(
            scale: CurvedAnimation(
              parent: _controller,
              curve: Interval(
                0,
                1 - index / widget.buttons.length,
                curve: Curves.easeOut
              ),
            ),
            child: widget.buttons[index],
          ),
        );
      })
      .toList()
      ..add(
        AnimatedBuilder(
          animation: _controller,
          builder: (BuildContext context, Widget? child) {
            return FloatingActionButton(
              child: Transform.rotate(
                angle: _controller.value * 0.5 * pi,
                alignment: FractionalOffset.center,
                child: Icon(_controller.isDismissed ? Icons.add : Icons.close),
              ),
              backgroundColor: _controller.isDismissed
                ? null
                : Theme.of(context).disabledColor,
              onPressed: () {
                if (widget.closeController.value == 0 
                  || widget.closeController.value == 1
                ) {
                  if (_controller.isDismissed) {
                    _controller.forward();
                  } else {
                    _controller.reverse();
                  }
                }
              }
            );
          },
        ),
      ),
    );
  }
  
  @override
  void dispose() {
    _controller.dispose();
    widget.closeController.removeListener(_close);
    super.dispose();
  }
}
