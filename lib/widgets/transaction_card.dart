import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';
import 'package:vaullet/ffi/proto/vaullet.ffi.pb.dart' as ffi;
import 'package:vaullet/functions/common.dart';

class TransactionCard extends StatelessWidget {
  const TransactionCard({Key? key, required this.transaction}) : super(key: key);

  final ffi.Transaction transaction;

  @override
  Widget build(BuildContext context) {
    final int diff = (transaction.postBalance - transaction.preBalance).toInt();
    return ListTile(
      leading: diff == 0
        ? const Icon(
          Icons.sync_alt
        )
        : diff > 0 
          ? const Icon(
            Icons.download,
            color: Colors.tealAccent,
            //size: 30.0,
          )
          : const Icon(
            Icons.upload,
            color: Colors.purpleAccent,
          ),
      title:  diff == 0
        ? Text(
          AppLocalizations.of(context)!.transactionUndefined
        )
        : diff > 0 
          ? Text(
            AppLocalizations.of(context)!.transactionReceive
          )
          : Text(
            AppLocalizations.of(context)!.transactionSend
          ),
      subtitle: Text(
        '${DateTime.fromMillisecondsSinceEpoch(transaction.timestamp.toInt() * 1000)}',
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      ),
      trailing: Text(displaySolAmount(diff, signed: true)),
    );
  }
}
