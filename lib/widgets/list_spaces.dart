import 'package:flutter/material.dart';

class ListSpaceTop extends StatelessWidget {
  const ListSpaceTop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 8,
    );
  }
}

class ListSpaceBottom extends StatelessWidget {
  const ListSpaceBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 80,
    );
  }
}
