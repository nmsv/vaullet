import 'dart:async';
import 'package:flutter/material.dart';
import 'package:vaullet/ffi/ffi.dart';
import 'package:vaullet/functions/common.dart';

class AccountCard extends StatefulWidget {
  const AccountCard({Key? key, required this.index}) : super(key: key);

  final int index;

  @override
  State<AccountCard> createState() => _AccountCardState();
}

class _AccountCardState extends State<AccountCard> {

  late final StreamSubscription _sub;
  String name = '';
  String balance = '';
  String address = '';
  bool isActive = false;

  void stateFromGlobal(state) => setState(() {
    final account = state.accounts[widget.index];
    name = account.name;
    balance = displaySolAmount(account.balance);
    address = account.address;
    isActive = state.activeAccount == widget.index;
  });

  @override
  void initState() {
    super.initState();

    Ffi().getState()
      .then(stateFromGlobal);

    _sub = Ffi().stateStream
      .distinct((prev, next) =>
        prev.accounts[widget.index] == next.accounts[widget.index]
          && ((prev.activeAccount == widget.index)
              == (next.activeAccount == widget.index))
      )
      .listen(stateFromGlobal);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 4, right: 8, left: 8),
      child: Card(
        color: isActive
          ? Theme.of(context).primaryColorLight
          : Theme.of(context).primaryColorDark,
        child: ListTile(
          leading: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 8),
                child: Icon(Icons.account_balance_wallet_rounded),
              ),
            ],
          ),
          title: Text(name),
          subtitle: Text(
            address,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            softWrap: false,
          ),
          trailing: Text(balance),
          onTap: isActive ? null : () {
            Ffi().setActiveAccount(widget.index);
          },
        ),
      ),
    );
  }
  
  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
