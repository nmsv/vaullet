use dart_bindgen::{config::*, Codegen};

fn main() {
    let mut prost_build = prost_build::Config::new();

    prost_build.protoc_arg("--experimental_allow_proto3_optional");

    prost_build
        .compile_protos(&["../../proto/vaullet.ffi.proto"], &["../../proto/"])
        .unwrap();

    prost_build
        .compile_protos(&["../../proto/vaullet.storage.proto"], &["../../proto/"])
        .unwrap();

    let crate_dir = std::env::var("CARGO_MANIFEST_DIR").unwrap();

    let mut config = cbindgen::Config {
        language: cbindgen::Language::C,
        ..Default::default()
    };

    config.braces = cbindgen::Braces::SameLine;

    config.cpp_compat = true;

    config.style = cbindgen::Style::Both;

    cbindgen::Builder::new()
        .with_crate(crate_dir)
        .with_config(config)
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file("binding.h");

    let config = DynamicLibraryConfig {
        ios: DynamicLibraryCreationMode::Executable.into(),
        android: DynamicLibraryCreationMode::open("libvaullet.so").into(),
        ..Default::default()
    };

    let codegen = Codegen::builder()
        .with_src_header("binding.h")
        .with_lib_name("libvaullet")
        .with_config(config)
        .with_allo_isolate()
        .build()
        .unwrap();

    let bindings = codegen.generate().unwrap();

    bindings.write_to_file("../../lib/ffi/ffi.g.dart").unwrap();
}
