#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void log_init(int64_t _port);

int32_t app_init(int64_t port, const char *docdir, const char *cachedir);

void get_state(int64_t port);

void state_subscribe(int64_t port);

void get_state_subscribe(int64_t port);

int32_t add_watch_account(const unsigned char *account_ptr, uintptr_t account_len, int64_t port);

int32_t add_signing_account(const unsigned char *account_ptr, uintptr_t account_len, int64_t port);

int32_t send_from_active_account(const unsigned char *new_transaction_ptr,
                                 uintptr_t new_transaction_len,
                                 int64_t port);

void set_active_account(uintptr_t acc_idx);

int32_t camera_submit_image(const unsigned char *image_ptr,
                            uintptr_t image_len,
                            uint32_t width,
                            uint32_t height);

void camera_get_result(int64_t port);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus
