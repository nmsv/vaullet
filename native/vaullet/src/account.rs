use std::{convert::TryInto, ops::Deref};

use tokio::task;

use solana_program::pubkey::Pubkey;

use solana_sdk::{
    derivation_path::DerivationPath,
    hash::Hash as BlockHash,
    message::Message,
    signer::{
        keypair::{self, Keypair},
        Signer,
    },
    system_instruction,
    transaction::Transaction,
};

use solana_client::nonblocking::rpc_client::RpcClient;

use aes::{
    cipher::{generic_array::GenericArray, BlockDecrypt, BlockEncrypt, KeyInit},
    Aes256Dec, Aes256Enc,
};

use scrypt::Params;

use rand_core::{OsRng, RngCore};

use prost::Message as _;

use crate::{
    vaullet_ffi::{
        Account as FfiAccount, NewAccountTransaction as FfiNewAccountTransaction,
        NewSigningAccount as FfiNewSigningAccount, NewWatchAccount as FfiNewWatchAccount,
    },
    vaullet_storage::{Account as StorageAccount, KeyPair as StorageKeyPair},
    Error,
};

#[derive(Clone, PartialEq)]
pub struct KeyPair {
    data: [u8; 64],
    hash_salt: [u8; 16],
}

impl KeyPair {
    pub fn from_storage(from: &StorageKeyPair) -> Result<Self, Error> {
        Ok(Self {
            data: from
                .data
                .as_slice()
                .try_into()
                .map_err(|_| Error::IncorrectKeypairFormat)?,
            hash_salt: from
                .hash_salt
                .as_slice()
                .try_into()
                .map_err(|_| Error::IncorrectKeypairFormat)?,
        })
    }

    pub fn into_storage(&self) -> StorageKeyPair {
        StorageKeyPair {
            data: self.data.to_vec(),
            hash_salt: self.hash_salt.to_vec(),
        }
    }

    pub async fn encrypt(mut data: [u8; 64], password: String) -> Result<Self, Error> {
        task::spawn_blocking(move || {
            let mut hash_salt = [0u8; 16];
            OsRng.fill_bytes(&mut hash_salt);

            let mut hash = [0u8; 32];

            scrypt::scrypt(
                password.as_bytes(),
                &hash_salt,
                &Params::recommended(),
                &mut hash,
            )
            .map_err(|_| Error::HashOutputBufferError)?;

            let cipher = Aes256Enc::new(&GenericArray::from(hash));

            for range in [0..16, 16..32, 32..48, 48..64].into_iter() {
                cipher.encrypt_block(GenericArray::from_mut_slice(&mut data[range]));
            }

            Ok(Self { data, hash_salt })
        })
        .await
        .map_err(|_| Error::JoinError)?
    }

    pub async fn sign_transaction(
        &self,
        mut transaction: Transaction,
        blockhash: BlockHash,
        password: String,
    ) -> Result<Transaction, Error> {
        let mut data = self.data.clone();
        let hash_salt = self.hash_salt.clone();
        task::spawn_blocking(move || {
            let keypair = {
                let mut hash = [0u8; 32];

                scrypt::scrypt(
                    password.as_bytes(),
                    &hash_salt,
                    &Params::recommended(),
                    &mut hash,
                )
                .map_err(|_| Error::HashOutputBufferError)?;

                let cipher = Aes256Dec::new(&GenericArray::from(hash));

                for range in [0..16, 16..32, 32..48, 48..64].into_iter() {
                    cipher.decrypt_block(GenericArray::from_mut_slice(&mut data[range]));
                }

                Keypair::from_bytes(&data).map_err(|_| Error::DecryptionError)?
            };

            transaction
                .try_partial_sign(&[&keypair], blockhash)
                .map_err(|_| Error::SigningError)?;

            Ok(transaction)
        })
        .await
        .map_err(|_| Error::JoinError)?
    }
}

#[derive(Clone, PartialEq)]
pub struct PublicKey {
    data: [u8; 32],
}

impl PublicKey {
    pub fn from_bytes(data: [u8; 32]) -> Self {
        Self { data }
    }

    pub fn as_bytes(&self) -> &[u8; 32] {
        &self.data
    }

    pub fn from_slice(data: &[u8]) -> Option<Self> {
        Some(Self::from_bytes(data.try_into().ok()?))
    }
}

impl PublicKey {
    pub fn to_vec(&self) -> Vec<u8> {
        self.data.to_vec()
    }

    pub fn from_native<T: Deref<Target = Pubkey>>(pk: T) -> Self {
        Self {
            data: pk.to_bytes(),
        }
    }

    pub fn into_native(&self) -> Pubkey {
        Pubkey::new_from_array(self.data)
    }

    pub fn into_bs58(&self) -> String {
        bs58::encode(self.data).into_string()
    }
}

#[derive(Clone)]
pub struct Account {
    pub name: String,
    pub address: PublicKey,
    pub keypair: Option<KeyPair>,
    pub rpc_http_address: String,
    pub rpc_ws_address: String,
    pub balance: Option<u64>,
}

impl Account {
    pub fn into_ffi(&self) -> FfiAccount {
        FfiAccount {
            name: self.name.to_owned(),
            address: self.address.into_bs58(),
            rpc_http_address: self.rpc_http_address.to_owned(),
            rpc_ws_address: self.rpc_ws_address.to_owned(),
            balance: self.balance,
            can_sign: self.keypair.is_some(),
        }
    }

    pub fn new_watch_account(from: FfiNewWatchAccount) -> Result<Self, Error> {
        let mut addr_buf = [0u8; 32];

        let len = bs58::decode(from.address)
            .into(&mut addr_buf)
            .map_err(|_| Error::IncorrectPublicKeyFormat)?;

        if len != 32 {
            return Err(Error::IncorrectPublicKeyFormat);
        }

        Ok(Self {
            name: from.name.to_owned(),
            address: PublicKey::from_bytes(addr_buf),
            keypair: None,
            rpc_http_address: from.rpc_http_address.to_owned(),
            rpc_ws_address: from.rpc_ws_address.to_owned(),
            balance: None,
        })
    }

    pub async fn new_signing_account(from: FfiNewSigningAccount) -> Result<Self, Error> {
        let seed = keypair::generate_seed_from_seed_phrase_and_passphrase(
            &from.seed_phrase,
            &from.passphrase,
        );

        let deriv_path = if from.derivation_path.is_empty() {
            None
        } else {
            Some(
                DerivationPath::from_key_str(&from.derivation_path)
                    .map_err(|_| Error::IncorrectDerivationPathFormat)?,
            )
        };

        let raw = keypair::keypair_from_seed_and_derivation_path(&seed, deriv_path)
            .map_err(|_| Error::IncorrectDerivationPathFormat)?;

        let address = PublicKey::from_native(&raw.pubkey());

        let keypair = Some(KeyPair::encrypt(raw.to_bytes(), from.encryption_password).await?);

        Ok(Self {
            name: from.name.to_owned(),
            address,
            keypair,
            rpc_http_address: from.rpc_http_address.to_owned(),
            rpc_ws_address: from.rpc_ws_address.to_owned(),
            balance: None,
        })
    }

    pub async fn send_transaction_from_ffi(
        &self,
        transaction_request: FfiNewAccountTransaction,
    ) -> Result<(), Error> {
        let keypair = self.keypair.as_ref().ok_or(Error::AccountCanNotSign)?;

        let destination = PublicKey::from_slice(
            &bs58::decode(transaction_request.destination)
                .into_vec()
                .map_err(|_| Error::IncorrectDestinationAddressFormat)?,
        )
        .ok_or(Error::IncorrectDestinationAddressFormat)?
        .into_native();

        let rpc_client = RpcClient::new(self.rpc_http_address.to_owned());

        let blockhash = rpc_client
            .get_latest_blockhash()
            .await
            .map_err(|_| Error::ConnectionError)?;

        let instruction = system_instruction::transfer(
            &self.address.into_native(),
            &destination,
            transaction_request.amount,
        );

        let transaction = keypair
            .sign_transaction(
                Transaction::new_unsigned(Message::new(
                    &[instruction],
                    Some(&self.address.into_native()),
                )),
                blockhash,
                transaction_request.password,
            )
            .await?;

        rpc_client
            .send_transaction(&transaction)
            .await
            .map_err(|_| Error::TransactionSendError)?;

        Ok(())
    }

    pub fn from_storage(from: &StorageAccount) -> Result<Self, Error> {
        Ok(Self {
            name: from.name.to_owned(),
            address: PublicKey::from_bytes(
                from.address
                    .clone()
                    .try_into()
                    .map_err(|_| Error::IncorrectPublicKeyFormat)?,
            ),
            keypair: from
                .keypair
                .as_ref()
                .map(|kp| KeyPair::from_storage(kp))
                .transpose()?,
            rpc_http_address: from.rpc_http_address.to_owned(),
            rpc_ws_address: from.rpc_ws_address.to_owned(),
            balance: None,
        })
    }

    pub fn into_storage(&self) -> StorageAccount {
        StorageAccount {
            name: self.name.to_owned(),
            address: self.address.to_vec(),
            keypair: self.keypair.as_ref().map(|kp| kp.into_storage()),
            rpc_http_address: self.rpc_http_address.to_owned(),
            rpc_ws_address: self.rpc_ws_address.to_owned(),
        }
    }

    pub fn encode_storage(&self) -> Vec<u8> {
        self.into_storage().encode_to_vec()
    }

    pub fn decode_storage(from: &[u8]) -> Result<Self, Error> {
        Self::from_storage(
            &StorageAccount::decode(from).map_err(|_| Error::IncorrectEncodingFormat)?,
        )
    }

    pub fn storage_fields_changed(&self, other: &Self) -> bool {
        self.name != other.name
            || self.address != other.address
            || self.keypair != other.keypair
            || self.rpc_http_address != other.rpc_http_address
            || self.rpc_ws_address != other.rpc_ws_address
    }

    pub fn sync_fields_changed(&self, other: &Self) -> bool {
        self.address != other.address
            || self.rpc_http_address != other.rpc_http_address
            || self.rpc_ws_address != other.rpc_ws_address
    }
}
