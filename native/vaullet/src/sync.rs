use std::{
    collections::BTreeMap,
    iter,
    sync::{
        mpsc::{self, Sender},
        Arc,
    },
    time::Duration,
};

use tokio::{sync::broadcast::error::RecvError, task::JoinHandle, time};

use futures::stream::StreamExt;

use solana_client::{
    nonblocking::{pubsub_client::PubsubClient, rpc_client::RpcClient},
    rpc_client::GetConfirmedSignaturesForAddress2Config,
};

use solana_sdk::signature::Signature as SolanaSignature;

use solana_transaction_status::UiTransactionEncoding;

use crate::{
    account::Account,
    database::TRANSACTIONS,
    state::State,
    transaction::{Signature, Transaction},
    CACHE, RT, STATE,
};

use log::{info, warn};

const RETRY_DELAY: Duration = Duration::from_secs(3);
const TRANSACTIONS_LIMIT: usize = 10;
const TRANSACTION_GET_TRIES: usize = 3;

pub fn spawn_transaction_cache_task(account: Arc<Account>) -> Sender<Arc<Transaction>> {
    let (tx, rx) = mpsc::channel::<Arc<Transaction>>();

    let _ = RT.spawn_blocking(move || {
        let cache = CACHE
            .read()
            .as_ref()
            .map(|c| c.open_tree(TRANSACTIONS))
            .or_else(|| {
                warn!("get_account_transactions: CACHE is not initialized");
                None
            })?
            .map_err(|err| {
                warn!("get_account_transactions: database error: {:?}", err);
            })
            .ok()?;

        while let Ok(transaction) = rx.recv() {
            let timestamp = transaction.timestamp.abs_diff(i64::MIN);

            let tr_db_key = [
                account.address.as_bytes().as_slice(),
                &timestamp.to_be_bytes(),
                &transaction.signature,
            ]
            .concat()
            .to_vec();

            let transaction = transaction.encode_storage();

            if let Err(err) = cache.insert(tr_db_key, transaction) {
                warn!("get_account_transactions: unable to store cache: {:?}", err);
            }
        }

        let _ = cache.flush();

        Some(())
    });

    tx
}

pub async fn account_subscribe(account_idx: usize, state: Arc<State>) {
    let account = match state.accounts.get(account_idx) {
        Some(a) => a,
        None => {
            info!(
                "account_subscribe: account {} not found,\
                cancelling subscription",
                account_idx
            );
            return;
        }
    };

    info!("account_subscribe: {} initializing sync", account_idx);

    let rpc_client = RpcClient::new(account.rpc_http_address.to_owned());

    let balance = loop {
        match rpc_client.get_balance(&account.address.into_native()).await {
            Err(err) => {
                warn!(
                    "account_subscribe: {} unable to get balance: {}",
                    account_idx, err
                );
                time::sleep(RETRY_DELAY).await;
            }
            Ok(b) => break b,
        }
    };

    let prev_account = account.clone();

    let res = STATE
        .set(move |prev| {
            let mut next = prev.clone();
            let mut next_account = next.accounts.get(account_idx)?.as_ref().clone();

            // Check just in case account parameters changed
            if next_account.address != prev_account.address {
                return None;
            }

            next_account.balance = Some(balance);

            *next.accounts.get_mut(account_idx)? = Arc::new(next_account);
            Some(next)
        })
        .await;

    if !res.changed {
        return;
    }

    loop {
        let sub_client = match PubsubClient::new(&account.rpc_ws_address.to_owned()).await {
            Ok(b) => b,
            Err(err) => {
                warn!(
                    "account_subscribe: {} unable open client: {}",
                    account_idx, err
                );
                time::sleep(RETRY_DELAY).await;
                continue;
            }
        };

        let (mut account_sub, account_unsub_fn) = match sub_client
            .account_subscribe(&account.address.into_native(), None)
            .await
        {
            Ok(b) => b,
            Err(err) => {
                warn!(
                    "account_subscribe: {} unable to subscribe: {}",
                    account_idx, err
                );
                time::sleep(RETRY_DELAY).await;
                continue;
            }
        };

        info!(
            "account_subscribe: {} initial sync complete, subscribing",
            account_idx
        );

        'sub: while let Some(resp) = account_sub.next().await {
            let prev_account = account.clone();

            let res = STATE
                .set(move |prev| {
                    let mut next = prev.clone();
                    let mut next_account = next.accounts.get(account_idx)?.as_ref().clone();

                    // Check just in case account parameters changed
                    if next_account.address != prev_account.address {
                        return None;
                    }

                    next_account.balance = Some(resp.value.lamports);

                    *next.accounts.get_mut(account_idx)? = Arc::new(next_account);
                    Some(next)
                })
                .await;

            if !res.changed {
                break 'sub;
            }
        }

        info!(
            "account_subscribe: {} subscription interrupted",
            account_idx
        );

        account_unsub_fn().await;
    }
}

pub async fn reload_account_cache_transactions(account: Arc<Account>) -> Option<Signature> {
    info!("get_account_transactions: reloading transactions from cache");

    let cache_transactions = RT
        .spawn_blocking(move || {
            let cache = CACHE
                .read()
                .as_ref()
                .map(|c| c.open_tree(TRANSACTIONS))
                .or_else(|| {
                    warn!("reload_account_cache_transactions: CACHE is not initialized");
                    None
                })?
                .map_err(|err| {
                    warn!(
                        "reload_account_cache_transactions: database error: {:?}",
                        err
                    );
                })
                .ok()?;

            let mut tr_iter = cache.scan_prefix(account.address.as_bytes());

            let list = iter::from_fn(|| tr_iter.next_back())
                .take(TRANSACTIONS_LIMIT)
                .filter_map(|res| {
                    let t = res
                        .ok()
                        .and_then(|(_, t)| Transaction::decode_storage(&t).ok())?;
                    Some(((t.timestamp, t.signature), Arc::new(t)))
                })
                .collect::<BTreeMap<_, _>>();

            Some(list)
        })
        .await;

    if let Ok(Some(transactions)) = cache_transactions {
        let latest = transactions
            .values()
            .next_back()
            .map(|t| t.signature.clone());

        STATE
            .set(move |prev| {
                Some(State {
                    transactions,
                    ..prev.clone()
                })
            })
            .await;

        latest
    } else {
        STATE
            .set(move |prev| {
                Some(State {
                    transactions: BTreeMap::new(),
                    ..prev.clone()
                })
            })
            .await;

        None
    }
}

pub async fn get_latest_cache_signature(account: Arc<Account>) -> Option<Signature> {
    RT.spawn_blocking(move || {
        let cache = CACHE
            .read()
            .as_ref()
            .map(|c| c.open_tree(TRANSACTIONS))
            .or_else(|| {
                warn!("get_latest_cache_signature: CACHE is not initialized");
                None
            })?
            .map_err(|err| {
                warn!("get_latest_cache_signature: database error: {:?}", err);
            })
            .ok()?;

        cache
            .scan_prefix(account.address.as_bytes())
            .next_back()
            .and_then(|res| {
                let t = res
                    .ok()
                    .and_then(|(_, t)| Transaction::decode_storage(&t).ok())?;
                Some(t.signature)
            })
    })
    .await
    .map_err(|_| {
        warn!("get_latest_cache_signature: task join error");
    })
    .ok()?
}

pub async fn get_account_new_transactions(
    account: Arc<Account>,
    latest_tr: Option<Signature>,
) -> bool {
    STATE
        .set(move |prev| {
            Some(State {
                transaction_loading: true,
                ..prev.clone()
            })
        })
        .await;

    info!("get_account_transactions: loading signatures");

    let rpc_client = RpcClient::new(account.rpc_http_address.to_owned());

    let signatures = match rpc_client
        .get_signatures_for_address_with_config(
            &account.address.into_native(),
            GetConfirmedSignaturesForAddress2Config {
                before: None,
                until: latest_tr
                    .as_ref()
                    .map(|t| SolanaSignature::new(t.as_slice())),
                limit: Some(TRANSACTIONS_LIMIT),
                commitment: None,
            },
        )
        .await
    {
        Err(err) => {
            warn!("get_account_transactions: unable to get balance: {}", err);
            time::sleep(RETRY_DELAY).await;
            return false;
        }
        Ok(b) => b,
    };

    let address_native = account.address.into_native();

    info!(
        "get_account_transactions: loaded {} signatures",
        signatures.len()
    );

    let mut signatures = signatures
        .into_iter()
        .filter_map(|s| match s.signature.parse() {
            Ok(s) => Some(s),
            Err(err) => {
                warn!(
                    "get_account_transactions: unable to parse signature: {:?}",
                    err
                );
                None
            }
        });

    info!("get_account_transactions: loading transactions");

    let tx = spawn_transaction_cache_task(account.clone());

    while let Some(signature) = signatures.next() {
        let mut transaction_tries = TRANSACTION_GET_TRIES;

        let transaction_raw = 'retries: loop {
            match rpc_client
                .get_transaction(&signature, UiTransactionEncoding::Binary)
                .await
            {
                Ok(t) => break 'retries t,
                Err(err) => {
                    if transaction_tries == 1 {
                        warn!(
                            "get_account_transactions: \
                              unable to get transaction, 0 tries left: {:?}",
                            err,
                        );

                        return false;
                    } else {
                        transaction_tries -= 1;
                        warn!(
                            "get_account_transactions: \
                            unable to get transaction, {} tries left: {:?}",
                            transaction_tries, err,
                        );
                    }
                }
            }
        };

        macro_rules! or_continue {
            ($s:expr) => {
                match $s {
                    Some(s) => s,
                    None => continue,
                }
            };
            ($s:expr, $t:expr) => {
                match $s {
                    Some(s) => s,
                    None => {
                        warn!("get_account_transactions: {}", $t);
                        continue;
                    }
                }
            };
        }

        let time = or_continue!(transaction_raw.block_time, "no block time");

        let message = or_continue!(
            transaction_raw.transaction.transaction.decode(),
            "unable to decode transaction"
        )
        .message;

        let meta = or_continue!(transaction_raw.transaction.meta, "no transaction metadata");

        let acc_idx = or_continue!(
            message
                .static_account_keys()
                .iter()
                .position(|a| *a == address_native),
            "no address in transaction"
        );

        let transaction = Arc::new(Transaction {
            signature: or_continue!(signature.try_into().ok(), "incorrect signature format"),
            account_address: account.address.clone(),
            timestamp: time,
            pre_balance: *or_continue!(
                meta.pre_balances.get(acc_idx),
                "address not found in pre_balances"
            ),
            post_balance: *or_continue!(
                meta.post_balances.get(acc_idx),
                "address not found in post_balances"
            ),
            status: meta.status.into(),
        });

        let _ = tx.send(transaction.clone());

        let account = account.clone();

        STATE
            .set(move |prev| {
                let mut next = prev.clone();
                let mut transactions = next.transactions.clone();

                if next
                    .accounts
                    .get(next.active_account)
                    .map(|a| &a.as_ref().address)
                    != Some(&account.address)
                {
                    return None;
                }

                transactions.insert((transaction.timestamp, transaction.signature), transaction);

                next.transactions = transactions;
                Some(next)
            })
            .await;
    }

    info!("get_account_transactions: finished loading transactions");

    STATE
        .set(move |prev| {
            Some(State {
                transaction_loading: false,
                ..prev.clone()
            })
        })
        .await;

    true
}

pub fn init() {
    // Account subscribe task:
    RT.spawn(async {
        let mut state_sub = STATE.subscribe();
        let mut tasks: Vec<JoinHandle<_>> = vec![];
        let mut prev_state: Option<Arc<State>> = None;

        loop {
            let state = if prev_state.is_some() {
                match state_sub.recv().await {
                    Ok(s) => s,
                    Err(RecvError::Lagged(_)) => continue,
                    Err(RecvError::Closed) => break,
                }
            } else {
                STATE.get().await
            };

            // Check if accounts changed in any relevant way
            // if any one did - replace/remove/add tasks

            let mut prev_iter = prev_state.as_mut().map(|accs| accs.accounts.iter());
            let mut curr_iter = state.accounts.iter();
            let mut i = 0;

            loop {
                match (prev_iter.as_mut().and_then(|p| p.next()), curr_iter.next()) {
                    (Some(prev), Some(curr)) => {
                        if prev.sync_fields_changed(curr) {
                            if let Some(task) = tasks.get_mut(i) {
                                task.abort();
                                *task = RT.spawn(account_subscribe(i, state.clone()));
                            }
                        }
                    }

                    (None, Some(_)) => {
                        tasks.push(RT.spawn(account_subscribe(i, state.clone())));
                    }

                    (Some(_), None) => {
                        if let Some(task) = tasks.pop() {
                            task.abort();
                        }
                    }

                    (None, None) => break,
                }

                i += 1;
            }

            prev_state = Some(state);
        }
    });

    // Transaction subscribe task (for selected account)
    RT.spawn(async {
        let mut state_sub = STATE.subscribe();
        let mut task: Option<JoinHandle<_>> = None;
        let mut prev_state: Option<Arc<State>> = None;

        loop {
            let state = if prev_state.is_some() {
                match state_sub.recv().await {
                    Ok(s) => s,
                    Err(RecvError::Lagged(_)) => continue,
                    Err(RecvError::Closed) => break,
                }
            } else {
                STATE.get().await
            };

            let prev_account = prev_state
                .as_ref()
                .and_then(|s| s.accounts.get(s.active_account));

            let curr_account = match state.accounts.get(state.active_account) {
                Some(a) => a.clone(),
                None => continue,
            };

            // Check if accounts changed in any relevant way
            // if any one did - replace/remove/add tasks

            let reload = if let Some(prev) = prev_account {
                prev.sync_fields_changed(&curr_account)
            } else {
                true
            };

            let update = if let Some(prev) = prev_account {
                prev.balance != curr_account.balance
            } else {
                false
            };

            if reload || update {
                if let Some(t) = task {
                    t.abort();
                }

                let latest_tr = if reload {
                    reload_account_cache_transactions(curr_account.clone()).await
                } else {
                    get_latest_cache_signature(curr_account.clone()).await
                };

                task = Some(RT.spawn(async move {
                    loop {
                        if get_account_new_transactions(curr_account.clone(), latest_tr).await {
                            break;
                        }
                    }
                }));
            }

            prev_state = Some(state);
        }
    });
}
