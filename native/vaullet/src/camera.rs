use std::panic;

use tokio::sync::broadcast::{self, error::RecvError, Sender};

use crate::RT;

use image::GrayImage;

use rqrr::PreparedImage;

pub struct CameraHub {
    tx: Sender<GrayImage>,
    rx: Sender<String>,
}

impl CameraHub {
    pub fn new() -> Self {
        let (img_tx, mut img_rx) = broadcast::channel(1);
        let (str_tx, _) = broadcast::channel(1);

        let str_tx_hub = str_tx.clone();

        RT.spawn(async move {
            loop {
                let img = match img_rx.recv().await {
                    Ok(i) => i,
                    Err(RecvError::Lagged(_)) => continue,
                    Err(RecvError::Closed) => break,
                };

                if let Ok(Some(s)) = panic::catch_unwind(|| {
                    let mut img = PreparedImage::prepare(img);
                    let grids = img.detect_grids();
                    let (_, content) = grids.into_iter().find_map(|i| i.decode().ok())?;
                    Some(content)
                }) {
                    let _ = str_tx.send(s);
                }
            }
        });

        CameraHub {
            tx: img_tx,
            rx: str_tx_hub,
        }
    }

    pub fn submit(&self, img: GrayImage) {
        let _ = self.tx.send(img);
    }

    pub async fn recv(&self) -> Option<String> {
        let mut rx = self.rx.subscribe();
        loop {
            match rx.recv().await {
                Ok(s) => return Some(s),
                Err(RecvError::Lagged(_)) => continue,
                Err(RecvError::Closed) => return None,
            }
        }
    }
}
