use tokio::sync::mpsc::UnboundedSender;

use allo_isolate::Isolate;

use log::{Level, LevelFilter, Metadata, Record};

use crate::RT;

struct Logger {
    tx: UnboundedSender<String>,
}

impl Logger {
    fn new(port: i64) -> Self {
        let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel();

        RT.spawn(async move {
            let isolate = Isolate::new(port);

            loop {
                let msg = match rx.recv().await {
                    Some(m) => m,
                    None => break,
                };

                if !isolate.post(msg) {
                    break;
                }
            }
        });

        Self { tx }
    }
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Info
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let _ = self.tx.send(format!(
                "[{}][{}:{}] {}",
                record.level(),
                record.module_path().unwrap_or_else(|| "?"),
                record
                    .line()
                    .map(|l| l.to_string())
                    .unwrap_or_else(|| "?".to_owned()),
                record.args(),
            ));
        }
    }

    fn flush(&self) {}
}

pub fn init(port: i64) {
    let _ = log::set_boxed_logger(Box::new(Logger::new(port)))
        .map(|()| log::set_max_level(LevelFilter::Info));
}
