use std::{collections::BTreeMap, iter, sync::Arc};

use tokio::sync::{
    broadcast::{self, Receiver as BroadcastReceiver, Sender as BroadcastSender},
    mpsc::{self, UnboundedSender},
    oneshot::{self, Sender as OneshotSender},
};

use prost::Message;

use crate::{
    account::Account,
    transaction::{Signature, Transaction},
    vaullet_ffi::State as FfiState,
    RT,
};

use log::error;

enum StateInput {
    Get(OneshotSender<Arc<State>>),
    Set {
        change: Box<dyn FnOnce(&State) -> Option<State> + Send>,
        channel: OneshotSender<StateSetResult>,
    },
}

pub struct StateSetResult {
    pub state: Arc<State>,
    pub changed: bool,
}

#[derive(Clone)]
pub struct State {
    pub accounts: Vec<Arc<Account>>,
    pub active_account: usize,
    pub transaction_loading: bool,
    pub transactions: BTreeMap<(i64, Signature), Arc<Transaction>>,
}

impl State {
    pub fn into_ffi(&self) -> FfiState {
        let mut tr_iter = self.transactions.values();
        FfiState {
            accounts: self.accounts.iter().map(|a| a.into_ffi()).collect(),
            active_account: Some(self.active_account as u32),
            transaction_loading: self.transaction_loading,
            transactions: iter::from_fn(|| tr_iter.next_back())
                .map(|a| a.into_ffi())
                .collect(),
        }
    }

    pub fn encode_ffi(&self) -> Vec<u8> {
        self.into_ffi().encode_to_vec()
    }
}

impl Default for State {
    fn default() -> Self {
        State {
            active_account: 0,
            accounts: vec![],
            transaction_loading: false,
            transactions: BTreeMap::new(),
        }
    }
}

pub struct StateHub {
    input: UnboundedSender<StateInput>,
    output: BroadcastSender<Arc<State>>,
}

impl StateHub {
    pub fn new(init: State) -> Self {
        let (input_tx, mut input_rx) = mpsc::unbounded_channel();
        let (output, _) = broadcast::channel(1);
        let hub_output = output.clone();

        RT.spawn(async move {
            let mut state = Arc::new(init);

            while let Some(input) = input_rx.recv().await {
                match input {
                    StateInput::Get(channel) => {
                        let _ = channel.send(state.clone());
                    }
                    StateInput::Set { change, channel } => {
                        if let Some(new_state) = change(state.as_ref()) {
                            state = Arc::new(new_state);
                            let _ = channel.send(StateSetResult {
                                state: state.clone(),
                                changed: true,
                            });
                            let _ = output.send(state.clone());
                        } else {
                            let _ = channel.send(StateSetResult {
                                state: state.clone(),
                                changed: false,
                            });
                        }
                    }
                }
            }
        });

        StateHub {
            input: input_tx,
            output: hub_output,
        }
    }

    pub async fn get(&self) -> Arc<State> {
        let (tx, rx) = oneshot::channel();
        let _ = self.input.send(StateInput::Get(tx));
        rx.await
            .map_err(|_| {
                error!("get: state task not spawned");
            })
            .unwrap()
    }

    pub async fn set<F: 'static + FnOnce(&State) -> Option<State> + Send>(
        &self,
        change: F,
    ) -> StateSetResult {
        let (tx, rx) = oneshot::channel();
        let _ = self.input.send(StateInput::Set {
            change: Box::new(change),
            channel: tx,
        });
        rx.await
            .map_err(|_| {
                error!("get: state task not spawned");
            })
            .unwrap()
    }

    pub fn subscribe(&self) -> BroadcastReceiver<Arc<State>> {
        self.output.subscribe()
    }
}
