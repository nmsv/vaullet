use std::{path::PathBuf, sync::Arc};

use tokio::{fs, sync::broadcast::error::RecvError, task};

use crate::{account::Account, state::State, Error, CACHE, DATA, RT, STATE};

//const SETTINGS: &[u8] = &[1];
pub const ACCOUNTS: &[u8] = &[3];
pub const TRANSACTIONS: &[u8] = &[4];

pub async fn init(mut docdir: PathBuf, mut cachedir: PathBuf) -> Result<(), Error> {
    fs::metadata(&docdir)
        .await
        .map_err(|_| Error::DocDirAccessError)?;

    docdir.push("data.db");

    fs::metadata(&cachedir)
        .await
        .map_err(|_| Error::CacheDirAccessError)?;

    cachedir.push("cache.db");

    let data = task::spawn_blocking(move || sled::open(docdir))
        .await
        .map_err(|_| Error::SpawnBlockingError)?
        .map_err(|_| Error::DataDbOpenError)?;

    let cache = task::spawn_blocking(move || sled::open(cachedir))
        .await
        .map_err(|_| Error::SpawnBlockingError)?
        .map_err(|_| Error::CacheDbOpenError)?;

    {
        *DATA.write() = Some(data);
    }
    {
        *CACHE.write() = Some(cache);
    }

    // Restore data on start
    RT.spawn(async {
        let accounts = task::spawn_blocking(|| {
            let vec: Result<_, Error> = Ok(DATA
                .read()
                .as_ref()
                .ok_or(Error::UnknownError)?
                .open_tree(ACCOUNTS)
                .map_err(|_| Error::OpenDataTreeError)?
                .iter()
                .filter_map(|res| {
                    let (_, account) = res.ok()?;
                    Account::decode_storage(&account).ok()
                })
                .map(|acc| Arc::new(acc))
                .collect());
            vec
        })
        .await
        .map_err(|_| Error::SpawnBlockingError)??;

        STATE
            .set(move |prev| {
                let mut next = prev.clone();
                next.accounts = accounts;
                Some(next)
            })
            .await;

        let res: Result<_, Error> = Ok(());

        res
    })
    .await
    .map_err(|_| Error::JoinError)??;

    // Subscribe for state changes
    RT.spawn(async {
        let mut state_sub = STATE.subscribe();
        let mut prev_state: Option<Arc<State>> = None;

        loop {
            let state = match state_sub.recv().await {
                Ok(s) => s,
                Err(RecvError::Lagged(_)) => continue,
                Err(RecvError::Closed) => break,
            };

            let state_stash = state.clone();

            let _ = RT
                .spawn_blocking(move || {
                    let data = match DATA
                        .read()
                        .as_ref()
                        .and_then(|d| d.open_tree(ACCOUNTS).ok())
                    {
                        Some(d) => d,
                        None => return,
                    };

                    let _ = data.transaction::<_, (), ()>(|tx| {
                        if let Some(prev) = prev_state.as_ref() {
                            if prev.accounts.len() > state.accounts.len() {
                                for i in state.accounts.len()..prev.accounts.len() {
                                    let _ = tx.remove(&i.to_be_bytes());
                                }
                            }
                        }

                        for (i, acc) in state.accounts.iter().enumerate() {
                            if prev_state
                                .as_ref()
                                .and_then(|s| s.accounts.get(i))
                                .map(|prev| prev.storage_fields_changed(acc))
                                .unwrap_or(true)
                            {
                                let acc_storage = acc.encode_storage();
                                let _ = tx.insert(&i.to_be_bytes(), acc_storage);
                            }
                        }

                        Ok(())
                    });

                    let _ = data.flush();
                })
                .await;

            prev_state = Some(state_stash);
        }
    });

    Ok(())
}
