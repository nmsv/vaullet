use std::{ffi::CStr, os::raw, path::PathBuf, slice, sync::Arc};

use tokio::{runtime::Runtime, sync::broadcast::error::RecvError};

use lazy_static::lazy_static;

use allo_isolate::Isolate;

use state::{State, StateHub};

use account::Account;

use sled::Db;

use parking_lot::RwLock;

use prost::Message as _;

use image::GrayImage;

use camera::CameraHub;

use vaullet_ffi::{
    NewAccountTransaction as FfiNewAccountTransaction, NewSigningAccount as FfiNewSigningAccount,
    NewWatchAccount as FfiNewWatchAccount,
};

pub use vaullet_ffi::Error;

pub mod vaullet_ffi {
    include!(concat!(env!("OUT_DIR"), "/vaullet.ffi.rs"));
}
pub mod vaullet_storage {
    include!(concat!(env!("OUT_DIR"), "/vaullet.storage.rs"));
}
mod account;
mod camera;
mod database;
mod state;
mod sync;
mod transaction;

#[cfg(feature = "logger")]
mod logger;

macro_rules! null_check {
    ($ptr:expr) => {
        if $ptr.is_null() {
            return Error::NullPointerInput.into();
        }
    };
}

lazy_static! {
    static ref RT: Runtime = Runtime::new().unwrap();
    static ref STATE: StateHub = StateHub::new(State::default());
    static ref DATA: RwLock<Option<Db>> = RwLock::new(None);
    static ref CACHE: RwLock<Option<Db>> = RwLock::new(None);
    static ref CAMERA: CameraHub = CameraHub::new();
}

async fn async_init(docdir: PathBuf, cachedir: PathBuf) -> Result<(), Error> {
    database::init(docdir, cachedir).await?;
    sync::init();

    Ok(())
}

#[no_mangle]
pub extern "C" fn log_init(_port: i64) {
    #[cfg(feature = "logger")]
    logger::init(_port);
}

#[no_mangle]
pub extern "C" fn app_init(
    port: i64,
    docdir: *const raw::c_char,
    cachedir: *const raw::c_char,
) -> i32 {
    null_check!(docdir);
    null_check!(cachedir);

    let docdir = match unsafe { CStr::from_ptr(docdir).to_str() } {
        Ok(s) => s,
        Err(_) => return Error::StrConvertError.into(),
    };

    let docdir: PathBuf = match docdir.parse() {
        Ok(p) => p,
        Err(_) => return Error::PathParseError.into(),
    };

    let cachedir = match unsafe { CStr::from_ptr(cachedir).to_str() } {
        Ok(s) => s,
        Err(_) => return Error::StrConvertError.into(),
    };

    let cachedir: PathBuf = match cachedir.parse() {
        Ok(p) => p,
        Err(_) => return Error::PathParseError.into(),
    };

    let task = Isolate::new(port).task(async move {
        match async_init(docdir, cachedir).await {
            Ok(_) => 0,
            Err(err) => err.into(),
        }
    });

    RT.spawn(task);

    0
}

#[no_mangle]
pub extern "C" fn get_state(port: i64) {
    let t = Isolate::new(port).task(async move {
        let state = STATE.get().await.encode_ffi();

        state
    });

    RT.spawn(t);
}

#[no_mangle]
pub extern "C" fn state_subscribe(port: i64) {
    RT.spawn(async move {
        let isolate = Isolate::new(port);

        let mut state_sub = STATE.subscribe();

        loop {
            let state = match state_sub.recv().await {
                Ok(s) => s.encode_ffi(),
                Err(RecvError::Lagged(_)) => continue,
                Err(RecvError::Closed) => break,
            };

            if !isolate.post(state) {
                break;
            }
        }
    });
}

#[no_mangle]
pub extern "C" fn get_state_subscribe(port: i64) -> () {
    RT.spawn(async move {
        let isolate = Isolate::new(port);

        if isolate.post(STATE.get().await.encode_ffi()) {
            let mut state_sub = STATE.subscribe();

            loop {
                let state = match state_sub.recv().await {
                    Ok(s) => s.encode_ffi(),
                    Err(RecvError::Lagged(_)) => continue,
                    Err(RecvError::Closed) => break,
                };

                if !isolate.post(state) {
                    break;
                }
            }
        }
    });
}

#[no_mangle]
pub extern "C" fn add_watch_account(
    account_ptr: *const raw::c_uchar,
    account_len: usize,
    port: i64,
) -> i32 {
    null_check!(account_ptr);

    let account_buf = unsafe { slice::from_raw_parts(account_ptr, account_len) };

    let account = match FfiNewWatchAccount::decode(account_buf) {
        Ok(o) => o,
        Err(_) => return Error::IncorrectEncodingFormat.into(),
    };

    let task = Isolate::new(port).task(async move {
        match Account::new_watch_account(account) {
            Ok(account) => {
                STATE
                    .set(move |prev| {
                        let mut next: State = prev.clone();
                        next.accounts.push(Arc::new(account));
                        Some(next)
                    })
                    .await;

                0
            }
            Err(err) => err.into(),
        }
    });

    RT.spawn(task);

    0
}

#[no_mangle]
pub extern "C" fn add_signing_account(
    account_ptr: *const raw::c_uchar,
    account_len: usize,
    port: i64,
) -> i32 {
    null_check!(account_ptr);

    let account_buf = unsafe { slice::from_raw_parts(account_ptr, account_len) };

    let account = match FfiNewSigningAccount::decode(account_buf) {
        Ok(o) => o,
        Err(_) => return Error::IncorrectEncodingFormat.into(),
    };

    let task = Isolate::new(port).task(async move {
        match Account::new_signing_account(account).await {
            Ok(account) => {
                STATE
                    .set(move |prev| {
                        let mut next: State = prev.clone();
                        next.accounts.push(Arc::new(account));
                        Some(next)
                    })
                    .await;

                0
            }
            Err(err) => err.into(),
        }
    });

    RT.spawn(task);

    0
}

#[no_mangle]
pub extern "C" fn send_from_active_account(
    new_transaction_ptr: *const raw::c_uchar,
    new_transaction_len: usize,
    port: i64,
) -> i32 {
    null_check!(new_transaction_ptr);

    let new_transaction_buf =
        unsafe { slice::from_raw_parts(new_transaction_ptr, new_transaction_len) };

    let new_transaction = match FfiNewAccountTransaction::decode(new_transaction_buf) {
        Ok(o) => o,
        Err(_) => return Error::IncorrectEncodingFormat.into(),
    };

    let task = Isolate::new(port).task(async move {
        let state = STATE.get().await;

        let res = match state.accounts.get(state.active_account) {
            Some(account) => account.send_transaction_from_ffi(new_transaction).await,
            None => Err(Error::NoActiveAccount),
        };

        match res {
            Ok(_) => 0,
            Err(err) => err.into(),
        }
    });

    RT.spawn(task);

    0
}

#[no_mangle]
pub extern "C" fn set_active_account(acc_idx: usize) {
    RT.spawn(async move {
        STATE
            .set(move |prev| {
                let mut next: State = prev.clone();
                next.accounts.get(acc_idx)?;
                next.active_account = acc_idx;
                Some(next)
            })
            .await;
    });
}

#[no_mangle]
pub extern "C" fn camera_submit_image(
    image_ptr: *const raw::c_uchar,
    image_len: usize,
    width: u32,
    height: u32,
) -> i32 {
    null_check!(image_ptr);

    let image_buf = unsafe { slice::from_raw_parts(image_ptr, image_len) };

    let image = match GrayImage::from_vec(width, height, image_buf.to_vec()) {
        Some(o) => o,
        None => return Error::IncorrectEncodingFormat.into(),
    };

    CAMERA.submit(image);

    0
}

#[no_mangle]
pub extern "C" fn camera_get_result(port: i64) {
    let task = Isolate::new(port).task(async move { CAMERA.recv().await });

    RT.spawn(task);
}
