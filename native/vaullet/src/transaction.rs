use std::convert::TryInto;

use prost::Message;

use solana_sdk::{signature::SIGNATURE_BYTES, transaction::Result as NativeTransactionResult};

use crate::{
    account::PublicKey, vaullet_ffi::Transaction as FfiTransaction,
    vaullet_storage::Transaction as StorageTransaction, Error,
};

pub type Signature = [u8; SIGNATURE_BYTES];

pub enum TransactionStatus {
    Ok,
    Err,
}

impl<T> From<NativeTransactionResult<T>> for TransactionStatus {
    fn from(from: NativeTransactionResult<T>) -> Self {
        match from {
            Ok(_) => TransactionStatus::Ok,
            Err(_) => TransactionStatus::Err,
        }
    }
}

impl TransactionStatus {
    fn from_u32(from: u32) -> Option<Self> {
        match from {
            0 => Some(Self::Ok),
            1 => Some(Self::Err),
            _ => None,
        }
    }

    fn into_u32(&self) -> u32 {
        match self {
            TransactionStatus::Ok => 0,
            TransactionStatus::Err => 1,
        }
    }
}

pub struct Transaction {
    pub signature: Signature,
    pub account_address: PublicKey,
    pub timestamp: i64,
    pub status: TransactionStatus,
    pub pre_balance: u64,
    pub post_balance: u64,
}

impl Transaction {
    pub fn into_ffi(&self) -> FfiTransaction {
        FfiTransaction {
            signature: bs58::encode(&self.signature).into_string(),
            account_address: self.account_address.into_bs58(),
            timestamp: self.timestamp,
            status: self.status.into_u32(),
            pre_balance: self.pre_balance,
            post_balance: self.post_balance,
        }
    }

    pub fn from_storage(from: &StorageTransaction) -> Result<Self, Error> {
        Ok(Self {
            signature: from
                .signature
                .clone()
                .try_into()
                .map_err(|_| Error::IncorrectSignatureFormat)?,
            account_address: PublicKey::from_bytes(
                from.account_address
                    .clone()
                    .try_into()
                    .map_err(|_| Error::IncorrectPublicKeyFormat)?,
            ),
            timestamp: from.timestamp,
            status: TransactionStatus::from_u32(from.status)
                .ok_or_else(|| Error::IncorrectTransactionStatus)?,
            pre_balance: from.pre_balance,
            post_balance: from.post_balance,
        })
    }

    pub fn into_storage(&self) -> StorageTransaction {
        StorageTransaction {
            signature: self.signature.to_vec(),
            account_address: self.account_address.to_vec(),
            timestamp: self.timestamp,
            status: self.status.into_u32(),
            pre_balance: self.pre_balance,
            post_balance: self.post_balance,
        }
    }

    pub fn encode_storage(&self) -> Vec<u8> {
        self.into_storage().encode_to_vec()
    }

    pub fn decode_storage(from: &[u8]) -> Result<Self, Error> {
        Self::from_storage(
            &StorageTransaction::decode(from).map_err(|_| Error::IncorrectEncodingFormat)?,
        )
    }
}
